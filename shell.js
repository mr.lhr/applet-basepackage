/**
 * shell 脚本
 * by: mr.lhr
 * time: 2020年6月8日 14点23分
 */


 const config = require('./shellconfig.json')
 let SWAGGERURL = config && config.SWAGGERURL ? config.SWAGGERURL :'http://192.168.3.220:8202/v2/api-docs'
 let SWAGGERSPACE = ''
 let GITOBJ = config && config.GITOBJ ? config.GITOBJ : {
   branch: 0,
   path: 'gitlab.com/mr.lhr/applet-basepackage.git'
 }
 
  const request = require('request')
  const program = require('commander')
  const ora = require('ora')
  const chalk = require('chalk')
  const shell = require('shelljs')
  const spinner = ora('running')
  const fs = require('fs')
  const path = require('path')
  
  const FILEOBJ = {
    /**
     * pages name
     */
    name: '',
    /**
     * yes or no to del dir
     */
    isDelDir: 1,
    /**
     * pulldown
     */
    pullDown: 0,
    /**
     * ReachBottom
     */
    reachBottom: 0
  }
  
  /**
   * api
   */
  const RESTAPI = () => {
    console.log(chalk.gray('RESTAPI'))
    spinner.start('RESTAPI handleing...')
    const fileBox = path.resolve('./src/api')
    const boxArr = fs.readdirSync(fileBox)
    let filePath = '' // path.resolve('./src/api/model')
    let pathArr = '' // fs.readdirSync(filePath)
    const rw = fs.createWriteStream('./src/api/RESTFULLURL.ts')
    const rwModel = fs.createWriteStream('./src/api/apiIndex.d.ts')
    const importArr = []
    const exportArr = []
    const modelArr = []
    boxArr.map(boxItem => {
      if (boxItem.includes('.')) return false
      filePath = path.resolve(`./src/api/${boxItem}`)
      pathArr = fs.readdirSync(filePath)
      pathArr.map(item => {
        if (item.includes('.')) return false
        importArr.push(`import ${item} from './${boxItem}/${item}'`)
        exportArr.push(`  ...${item}`)
        modelArr.push(`///<reference path="./${boxItem}/${item}/index.d.ts" />`)
      })
    })
   
    if (importArr) {
      const rwStr = `${importArr.join('\n')}\n\nexport default {\n${exportArr.join(',\n')}\n}\n`
      rw.write(rwStr)
      rwModel.write(`${modelArr.join('\n')}\n`)
    }
  
    spinner.succeed('RESTAPI done')
    return true
  }
  
  /**
   * router
   */
  const ROUTER = () => {
    console.log(chalk.gray('ROUTER'))
    spinner.start('ROUTER handleing...')
    const filePath = path.resolve('./src/router')
    const pathArr = fs.readdirSync(filePath)
    const rw = fs.createWriteStream('./src/router/appendRouter.ts')
    const importArr = []
    const exportArr = []
    pathArr.map(item => {
      if (item.includes('.')) return false
      importArr.push(`import ${item} from './${item}'`)
      exportArr.push(`  ...${item}`)
    })
    if (importArr) {
      const rwStr = `${importArr.join('\n')}\n\nexport default [\n${exportArr.join(',\n')}\n]\n`
      rw.write(rwStr)
    }
    spinner.succeed('ROUTER done')
    return true
  }
  
  /**
   * upgrade
   */
  const UPGRADE = async() => {
    console.log(chalk.gray('template start download'))
    spinner.start('downloading')
    shell.rm('-rf', 'basePackage')
    const gitPath = `${GITOBJ.branch === 0 ? '' : `-b ${GITOBJ.branch}`} ${GITOBJ.path.includes('http://') || GITOBJ.path.includes('https://') ? `${GITOBJ.path}`: `http://${GITOBJ.path}` }` 
    const { code, stdout, stderr } = await shell.exec(`git clone ${gitPath} basePackage`)
    if (code !== 0) {
      console.log(chalk.red('components install fail'))
      console.log(chalk.red('Exit code:', code))
      console.log(chalk.red('output:', stdout))
      console.log(chalk.red('stderr:', stderr))
      return spinner.fail('fail')
    }
    const bPackage = require('./basePackage/package.json')
    const nPackage = require('./package.json')
    let isInstall = false
    if (bPackage && nPackage) {
      for (const key in bPackage.dependencies) {
        if (!nPackage.dependencies[key]) {
          nPackage.dependencies[key] = bPackage.dependencies[key]
          isInstall = true
        }
      }
      for (const key in bPackage.devDependencies) {
        if (!nPackage.devDependencies[key]) {
          nPackage.devDependencies[key] = bPackage.devDependencies[key]
          isInstall = true
        }
      }
      if (bPackage.upgrade && bPackage.upgrade.path && bPackage.upgrade.path.length > 0) {
        for (let i = 0; i < bPackage.upgrade.path.length; i++) {
          const item = bPackage.upgrade.path[i]
          await shell.rm('-rf', item)
          await shell.cp('-rf', `basePackage/${item}`, item)
          if (item.includes('/api/model')) await RESTAPI()
          if (item.includes('router/')) await ROUTER()
        }
      }
      nPackage.upgrades ? nPackage.upgrades[bPackage.name] = bPackage.version : nPackage.upgrades = { [bPackage.name]: bPackage.version }
      const rw = fs.createWriteStream('./package.json')
      await rw.write(JSON.stringify(nPackage, null, '\t') + '\n', async() => {
        if (isInstall) {
          await shell.exec('yarn install')
        }
      })
    }
    await shell.rm('-rf', 'basePackage')
    spinner.succeed('complete')
  }
  
  const requestAsync = function(url) {
    return new Promise(function(resolve, reject) {
      request.get(url, (err, response, body) => {
        if (err) return reject(err)
        resolve(body && typeof body !== 'object' ? JSON.parse(body) : body)
      })
    })
  }
  
  /**
   * 获取日期
   */
  const addZore = (str) => {
    str = `${str}`
    return str.length > 1 ? str : `0${str}`
  }
  const getTimeStr = () => {
    const now = new Date()
    return `${now.getFullYear()}-${addZore(now.getMonth() + 1)}-${addZore(now.getDate())} ${addZore(now.getHours())}:${addZore(now.getMinutes())}:${addZore(now.getSeconds())}`
  }

  /**
   * swagger
   */
  const handlePaths = (APIKey, modelName, modelNameT) => {
    const apiKeyArr = APIKey.split('/')
    const len = apiKeyArr.length
    const spanName = SWAGGERSPACE || ''
    let _apiKey =  `_${modelNameT}${APIKey.replace(/{|}/g, '').replace(/\-/g, '').replace(/\//g, '_')}` //len > 0 ? `_${spanName}${apiKeyArr[0]}` : ''
    let faceName = len >= 2 && apiKeyArr[len - 2].toLocaleLowerCase() !== modelName.toLocaleLowerCase() ? `${modelName}${apiKeyArr[len - 1]}` : apiKeyArr[len - 1]
    const params = []
  
    if (/{.*}/.test(_apiKey)) _apiKey = _apiKey.replace(/{|}/g, '').replace(/\-/g, '')
    if (/{.*}/.test(faceName))  {
      faceName = `${modelName}${apiKeyArr[len - 2]}${apiKeyArr[len - 1]}`
      faceName = faceName.replace(/{|}/g, '').replace(/\-/g, '')
     }
    return { apiKeyArr, _apiKey, faceName, params }
  }
  const SWAGGER = async() => {
    console.log(chalk.gray('get swagger start'))
    spinner.start('getting\n')
    console.info(SWAGGERURL)
    const res = await requestAsync(SWAGGERURL)
    if (!res) return spinner.fail('get swagger error')
    const { tags, paths, definitions } = res
    const fileModel = {}
    tags.map(item => {
      fileModel[item.name] = {
        // path: item.name.replace(/Controller|\s*/g, '').replace(/[\u4e00-\u9fa5]/g, '').replace(/\-/g, '').replace(/\(|\)|（|）/g, '').toLowerCase() 
        path: item.description.replace(/Controller|\s*/g, '').replace(/\-/g, '').replace(/[\u4e00-\u9fa5]/g, '').replace(/\(|\)|（|）/g, '').toLowerCase() 
      }
    })
    const spacename = (SWAGGERSPACE || 'model').replace(/[\u4e00-\u9fa5]/g, '').replace(/\(|\)|（|）/g, '').replace(/\-/g, '')
  
    const returnEts = (ref) => {
      if (/Query.*DTO/.test(ref)) return { ets: 'extends _default.listPagePost ', del: ['currentPage', 'reqCode', 'reqTime', 'showCount'] }
      if (/^(?!Query).*DTO/.test(ref)) return { ets: 'extends _default.post ', del: ['reqCode', 'reqTime'] }
      if (/ResultBean.*/.test(ref)) return { ets: 'extends _default.res ', del: ['code', 'msg', 'timespan'] }
      if (/Request/.test(ref)) return { ets: 'extends _default.post ', del: ['reqCode', 'reqTime'] }
      return { ets: '', del: [] }
    }
    const changeKeytype = {
     string: 'string',
     integer: 'number',
     number: 'number',
     boolean: 'boolean'
    }
    const changeKey = async(refs, space, medel, params, faceName) => {
      if (params && params.length > 0) {
        const key = `${faceName}Params`
        medel[key] = [`  interface ${key} {`]
        params.map(item => {
          medel[key].push(`    ${item}: string,`)
        })
        medel[key].push('  }')
        return `extends ${space}.${key} `
      }
      // a
      if (!refs) return ''
      const scheamArr = refs.split('/')
      const key = scheamArr[scheamArr.length - 1]
      const mkey = key.replace(/«|»/g, '').replace(/\[|\]/g, '').replace(/\-/g, '')
      const obj = definitions[key]
      if (!obj || !obj.properties) return {}
      if (!medel[key]) {
        const { ets, del } = returnEts(mkey)
        medel[key] = [`  interface ${mkey} ${ets}{`]
        for (const skey in obj.properties) {
          if (!del.includes(skey)) {
            const interItem = obj.properties[skey]
            let itemEts = ''
            if ((interItem.items && interItem.items.$ref) || (interItem && interItem.$ref)) {
              const $ref = interItem.$ref ? interItem.$ref : interItem.items.$ref
              const sscheamArr = $ref.split('/')
              const sskey = sscheamArr[sscheamArr.length - 1]
              const smkey = sskey.replace(/«|»/g, '').replace(/\-/g, '')
              itemEts = `${space}.${smkey}`
              await changeKey($ref, space, medel)
            }
            const arr = ['    /**', `     * ${interItem.description || ''}`, '     */']
            medel[key].push(...arr)
            let val = itemEts
            if (interItem.type === 'array' && itemEts) val = `${itemEts}[]`
            if (interItem.type === 'array' && !itemEts) val = `${changeKeytype[interItem.items.type]}[]`
            if (interItem.type !== 'array' && !itemEts) val = changeKeytype[interItem.type]
            if (interItem.type !== 'array') val = (val || '').replace(/\[|\]/g, '').replace(/\-/g, '')
            medel[key].push(`    ${skey}: ${val},`)
          }
        }
        if (medel[key].length > 1) {
          medel[key][medel[key].length - 1] = medel[key][medel[key].length - 1].substr(0, medel[key][medel[key].length - 1].length - 1)
        }
        medel[key].push('  }')
      }
      const ets = `extends ${space}.${mkey} `
      return ets
    }
    for (const key in paths) {
      const item = paths[key]
      const sitem = item.post || item.get
      let parameters = '' // sitem.parameters && sitem.parameters[0].schema && sitem.parameters[0].schema.$ref ? sitem.parameters[0].schema.$ref : ''
      let paramArr = []
      
      const responses = sitem.responses && sitem.responses[200] && sitem.responses[200].schema && sitem.responses[200].schema.$ref ? sitem.responses[200].schema.$ref : ''
      if (sitem.parameters && sitem.parameters.length > 0) {
        for (let i = 0; i < sitem.parameters.length; i++) {
          const temp = sitem.parameters[i]
          if (temp.schema && temp.schema.$ref) {
             parameters = temp.schema.$ref
             break
           }else {
            if (['query', 'path'].includes(temp.in)) paramArr.push(temp)
           }
        }
      }
  
      const joinObj = {
        method: item.post ? 'post' : 'get',
        name: sitem.summary,
        parameters,
        responses,
        paramArr
      }
      fileModel[sitem.tags].api ? fileModel[sitem.tags].api[key] = joinObj : fileModel[sitem.tags].api = { [key]: joinObj }
    }
  
    await shell.rm('-rf', `src/api/${spacename}/*`)
    for (const key in fileModel) {
      const item = fileModel[key]
      const modelName = spacename === 'model' ? item.path : `${spacename}${item.path}`
      const path = `src/api/${spacename}/${modelName}`.replace(/[\u4e00-\u9fa5]/g, '').replace(/\(|\)|（|）/g, '')
      console.log(chalk.gray(`-handleing ${key} path: ${path}`))
      await shell.mkdir('-p', path)
      const indexD = `${path}/index.d.ts`
      const index = `${path}/index.ts`
      await shell.touch(indexD)
      await shell.touch(index)
      const indexDrw = fs.createWriteStream(indexD)
      const indexrw = fs.createWriteStream(index)
      let indexrwDStr = []
      const indexrwDAStr = ['declare module _default {', '  interface apifuns {']
      const indexrwStr = ['export default {']
      const model = {}
      for (const APIKey in item.api) {
        console.log(chalk.gray(`                api: ${APIKey}`))
        const { _apiKey, faceName, params } = handlePaths(APIKey, item.path, modelName)
        const apiItem = item.api[APIKey]
        const aTitle = `   * ${apiItem.method} ${APIKey} ${apiItem.name}`
        const title = ['  /**', aTitle, '   */']
        // d.ts
        title[1] = `   * ${apiItem.name} ${item.api[APIKey].method}`
        indexrwDStr.push(...title)
        let ext = await changeKey(apiItem.parameters, `_${modelName}`, model, params, faceName)
        indexrwDStr.push(`  interface _p${faceName} ${ext}{`)
        if (apiItem.paramArr) {
         for (let i = 0; i < apiItem.paramArr.length; i ++) {
           let item = apiItem.paramArr[i]
           let arr = ['    /**', `     * ${item.description || ''}`, '     */']
           arr.push(`    ${item.name}${item.required ? '' : '?'}: ${changeKeytype[item.type]},`)
           indexrwDStr.push(...arr)
         }
       }
        indexrwDStr.push('  }')
        title[1] = `   * ${apiItem.name} res`
        indexrwDStr.push(...title)
        ext = await changeKey(apiItem.responses, `_${modelName}`, model)
        indexrwDStr.push(`  interface _r${faceName} ${ext}{`)
        indexrwDStr.push('  }')
        // api
        const api = ['    /**', `     * ${apiItem.name}`, '     */']
        indexrwDAStr.push(...api)
        indexrwDAStr.push(`    ${_apiKey}({ data, that }: { data: _${modelName}._p${faceName}, that?: any }): _${modelName}._r${faceName},`)
        // url
        title[1] = aTitle
        indexrwStr.push(...title)
        indexrwStr.push(`  ${_apiKey}: { method: '${apiItem.method}', url: '${APIKey}' },`)
      }
      indexrwStr[indexrwStr.length - 1] = indexrwStr[indexrwStr.length - 1].replace('},', '}')
      indexrwStr.push('}')
      indexrw.write(`${indexrwStr.join('\n')}\n`)
      indexrwDStr.push('}')
      if (indexrwDAStr.length > 1) {
        indexrwDAStr[indexrwDAStr.length - 1] = indexrwDAStr[indexrwDAStr.length - 1].substr(0, indexrwDAStr[indexrwDAStr.length - 1].length - 1)
      }
      indexrwDAStr.push('  }')
      indexrwDAStr.push('}')
      indexrwDStr.push(...indexrwDAStr)
      const modelArr = []
      for (const modelKey in model) {
        modelArr.push(...model[modelKey])
      }
      indexrwDStr = [...['/**', ` * ${key}`, ' * create: lihuarong<517849815@qq.com>', ` * time: ${getTimeStr()}`, ' */', `declare namespace _${modelName} {`], ...modelArr, ...indexrwDStr]
      indexDrw.write(`${indexrwDStr.join('\n')}\n`)
    }
    await RESTAPI()
    spinner.succeed('SWAGGER done')
    // console.info(JSON.stringify(fileModel, null, '\t'))
  }
  
  /**
   * FILE
   */
  const fToC = ([first, ...rest]) => `${first.toUpperCase()}${rest.join('')}`
  const handlePages = async(filePath, isDel) => {
    const pages = require('./src/pages.json')
    if (!pages || typeof pages !== 'object') return false
    if (!pages.pages) pages.pages = []
    let isWr = true
    let path = `pages/${filePath.split('.')[0]}`
    let index = -1
    for(let i = 0; i < pages.pages.length; i++) {
      const item =  pages.pages[i]
      if (item.path === path) { 
        isWr = false
        isDel ? index = i : ''
        break
      }
    }
    if (isWr || index !== -1) {
      let pushParam = {
        path,
        style: {}
      } 
      if (FILEOBJ.name) pushParam.style.navigationBarTitleText = FILEOBJ.name
      if (FILEOBJ.pullDown) pushParam.style.enablePullDownRefresh = true
      if (FILEOBJ.reachBottom) pushParam.style.onReachBottomDistance = parseInt(FILEOBJ.reachBottom)
      !isDel ? pages.pages.push(pushParam) : index !== -1 ? pages.pages.splice(index, 1) : ''
      const pagesRw = fs.createWriteStream('src/pages.json')
      await pagesRw.write(JSON.stringify(pages, null, '\t')+'\n')
    }
    return true
  }
  const getPath = (path) => {
    const pathArr = `${path}`.split('/')
    const filePath = pathArr.length === 1 ? `${pathArr[0]}.vue` : `${pathArr[pathArr.length - 2]}/${pathArr[pathArr.length - 1]}.vue`
    return { filePath, pathArr}
  }
  const FILE = async(path) => {
    if (!path) return console.log(chalk.gray(`-path is no null`))
    const { filePath, pathArr } = getPath(path)
    let fileAllPath = `src/pages/`
    const pull = FILEOBJ.pullDown ? '\n\n  onPullDownRefresh() {\n    // onPullDownRefresh\n  }' : ''
    const bottom = FILEOBJ.reachBottom ? '\n\n  onReachBottom() {\n    // onReachBottom\n  }' : ''
    const temp = `<template>\n  <view class="box"></view>\n</template>\n\n<script lang="ts">\nimport { Component, Vue } from "vue-property-decorator"\n@Component({})\nexport default class ${fToC(pathArr[pathArr.length - 1])} extends Vue {\n  onLoad() {\n    // onLoad\n  }\n\n  onShow() {\n    // onShow\n  }\n\n  onHide() {\n    // onHide\n  }${pull}${bottom}\n}\n</script>\n\n<style lang="scss">\n\n</style>\n`
    if(pathArr.length > 1) await shell.mkdir(`${fileAllPath}${pathArr[pathArr.length - 2]}`)
    fileAllPath = `${fileAllPath}${filePath}`
    const { code, stdout, stderr } = await shell.touch(fileAllPath)
    if (code !== 0) {
      console.log(chalk.red('FILE create fail'))
      console.log(chalk.red('Exit code:', code))
      console.log(chalk.red('output:', stdout))
      console.log(chalk.red('stderr:', stderr))
      return spinner.fail('fail')
    }
    const rw = fs.createWriteStream(fileAllPath)
    rw.write(temp)
    await handlePages(filePath)
    console.log(chalk.gray(`-handleing path: ${fileAllPath}`))
    spinner.succeed('FILE done')
  }
  
  const DFILE = async(path) => {
    const { filePath, pathArr } = getPath(path)
    await shell.rm('-rf',`src/pages/${filePath}`)
    await handlePages(filePath, true)
    let num = 0
    if (pathArr.length > 1) {
      const dir = `src/pages/${pathArr[pathArr.length - 2]}`
      await shell.ls(`${dir}/*.vue`).forEach(() => {
        num += 1
      })
      if (num === 0 && FILEOBJ.isDelDir) await shell.rm('-rf', dir)
    }
    spinner.succeed('DFILE done')
  }
  
  // vuex 
  const iNITVUX = () => {
    let vuexIndexImport = []
    let key = []
    let keyVal = ''
    const vuexIndexfirst = [`import Vue from 'vue'`, `import Vuex, { Store }  from 'vuex'`, 'Vue.use(Vuex)\n'] 
    const vuexIndexContent = [`\nexport interface IRootState {`]
    const vuexIndexEnd = [
      '}\n',
      '// Declare empty store first, dynamically register all modules later.',
      'const store: Store<IRootState> = new Vuex.Store<IRootState>({})',
      'export default store\n'
    ]
    const filePath = path.resolve('./src/store/modules')
    const pathArr = fs.readdirSync(filePath)
    pathArr.map(item => {
      if (!item.includes('.ts')) return false
      const name = item.split('.')[0]
      const tname = fToC(name)
      key.push(`  ${name}: I${tname}`)
      vuexIndexImport.push(`import { I${tname} } from './modules/${name}'`)
    })
    keyVal = key.join(',\n')
    const rw = fs.createWriteStream(`src/store/index.ts`)
    rw.write([...vuexIndexfirst, ...vuexIndexImport, ...vuexIndexContent, [keyVal], ...vuexIndexEnd].join('\n'))
    spinner.succeed('iNITVUX done')
  }
  
  const VUEX = async(name) => {
    const fileName = fToC(name)
    const modulesFile = ['/**', ` * ${FILEOBJ.name || name}`, ' * by: lihuarong', ` * time: ${getTimeStr()}`, ' */',
      `import { VuexModule, Module as vueMoudle, Action, Mutation, getModule } from 'vuex-module-decorators'`,
      `import store from '@/store'`,
      `import localStore from '@/store/store'`,
      `const key = '${name}' as string`,
      `export interface I${fileName} {`,
      `  ${name}: {}`,
      '}\n',
      '@vueMoudle({ dynamic: true, store, name: key })',
      `class ${fileName} extends VuexModule implements I${fileName} {`,
      `  public ${name} = localStore.get({ key }) || {}\n`,
      `  /**`,
      `   * 更新数据`,
      `   */`,
      `  @Mutation`,
      `  private async SET_${name.toUpperCase()}(value: object) {`,
      `    await localStore.set({ key, value })`,
      `    this.${name} = await localStore.get({ key })`,
      `    return true`,
      `  }\n`,
      `  /**`,
      `   * 删除数据`,
      `   */`,
      `  @Mutation`,
      `  private async DELETE_${name.toUpperCase()}(value: object = {}) {`,
      `    await localStore.set({ key, value })`,
      `    this.${name} = await localStore.get({ key })`,
      `    return true`,
      `  }\n`,
      `  /**`,
      `   * 更新数据`,
      `   */`,
      `  @Action`,
      `  public async update_${name}(value: object) {`,
      `    await this.SET_${name.toUpperCase()}(value)`,
      `    return true`,
      `  }\n`,
      `  /**`,
      `   * 删除数据`,
      `   */`,
      `  @Action`,
      `  public async delete_${name}() {`,
      `    await this.DELETE_${name.toUpperCase()}()`,
      `    return true`,
      `  }\n`,
      '}\n',
      `export const ${fileName}Module = getModule(${fileName})\n`
    ]
    const rw = fs.createWriteStream(`src/store/modules/${name}.ts`)
    rw.write(modulesFile.join('\n'))
    iNITVUX()
    spinner.succeed('VUEX done')
  }
  
  
  const VUEXD = async(name) => {
    await shell.rm('-rf', `src/store/modules/${name}.ts`)
    iNITVUX()
    spinner.succeed('VUEX done')
  }
  
  program.version('0.0.1', '-v, -version')
    .option('-e, --router', 'handle router')
    .option('-r, --restapi', 'handle restapi')
    .option('-u, --upgrade', 'upgrade template param:(branch: 0 or name, git)')
    .option('-s, --swagger', 'handel swagger')
    .option('-w, --swaggerDoc <url>', 'swaggerDoc url')
    .option('-n, --pagesName <name>', 'pages name')
    .option('-i, --delDir [is]', 'del dir')
    .option('-p, --pull [is]', 'pullDown ')
    .option('-b, --reachBottom <num>', 'reachBottom ')
    .option('-B, --branch <branch>', 'git branch')
    .option('-l, --giturl <url>', 'giturl')
    .option('-d, --filed <path>', 'handel del file')
    .option('-f, --file <path>', 'handel add file')
    .option('-x, --vuex <name>', 'create vuex file Temp')
    .option('-X, --vuexD <name>', 'del vuex file Temp')
    .action(async (option) => {
      // console.info(option)
    })
  
  
  program.on('option:router', () => ROUTER())
  
  program.on('option:restapi', () => RESTAPI())
  
  program.on('option:upgrade', () => UPGRADE())
  
  program.on('option:swagger', () => SWAGGER())
  
  program.on('option:file', (path) => FILE(path))
  
  program.on('option:filed', (path) => DFILE(path))
  
  program.on('option:pagesName', (name) => { 
    FILEOBJ.name = name
    SWAGGERSPACE = name
  })
  
  program.on('option:pull', (is) => { FILEOBJ.pullDown = /\d/.test(is) ? parseInt(is) : 1 })
  
  program.on('option:reachBottom', (num) => { FILEOBJ.reachBottom = num })
  
  program.on('option:delDir', (is) => FILEOBJ.isDelDir = /\d/.test(is) ? parseInt(is) : 1)
  
  program.on('option:branch', (branch) => GITOBJ.branch = branch )
  
  program.on('option:giturl', (url) => GITOBJ.path = url)
  
  program.on('option:swaggerDoc', (url) => SWAGGERURL = url)
  
  program.on('option:vuex', (name) => VUEX(name))
  
  program.on('option:vuexD', (name) => VUEXD(name))
  
  program.parse(process.argv)
  