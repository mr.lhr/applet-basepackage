
// 请求的域名地址
import { host as baseUrl } from '@/env'

import { UserinfoModule } from '@/store/modules/userinfo'

// 消息toast
// const showToast = ({ title = '微信api异常！', icon = 'none'}: { title?: string, icon?: 'none' | 'loading' | 'success'}) => {
//   uni.showToast({
//     title,
//     icon
//   })
// }

/**
 * this 模型
 */
interface thatModel {
  $API: _default.apifuns,
  $mp: {
    query: object,
    page: {
      onLoad: Function,
      onShow: Function
    }
  }
}

async function unirequest(url: string, options: any, that?: thatModel) {
  let userInfo = UserinfoModule.userInfo
  url = `${baseUrl}${url}`

  if(options.data) options.data.platformId = 1
  else options.data = { platformId: 1 }

  options.url = url
  options.header = { token: userInfo ? userInfo.token : '' }
  
  let respont: any = await uni.request(options)
  if (respont[0] && (respont[0].errMsg === 'request:fail' || respont[0].errMsg === 'request:fail ' )) return {}
  let { statusCode, data = {}, errMsg } = respont[1] || {}
  // if (statusCode != 200) uni.showToast({ title: errMsg, icon: 'none' })

  // 用户未注册 未登录
  // console.info(options.data, data, baseUrl, userInfo, that)
  if (data && data.code === '1101') {
    delete userInfo.token
    await  UserinfoModule.update_userinfo(userInfo)
    console.info(data, baseUrl, userInfo, that)
    return {}
  }
  if (data && data.code === '1100' && that) {
    if (userInfo.goLogin) {
      uni.hideLoading()
      uni.stopPullDownRefresh()
      return {}
    }
    userInfo.goLogin = true
    await  UserinfoModule.update_userinfo(userInfo)

    let loginData: any = await uni.login()
    if (!loginData[1] || loginData[1].errMsg != 'login:ok') {
      return {}
    }
    userInfo.jsCode = loginData[1].code
    
    const data = {} as _account._pwxLogin
    data.appKey = "1"
    data.jsCode = loginData[1].code
    const res: _account._rwxLogin = await that.$API._accountwxLogin({ data })
    if (res.code === '1101') {
      delete userInfo.token
      await  UserinfoModule.update_userinfo(userInfo)
      return {}
    }
    if (res.code !== '0000') {
      return {}
    } 
    userInfo.token = res.data ? res.data.token : ''
    userInfo.goLogin = false
    await  UserinfoModule.update_userinfo(userInfo)
    that.$mp.page.onLoad(that.$mp.query)
    that.$mp.page.onShow()
  }
  uni.stopPullDownRefresh()
  return data
}

export default async function request(url: string, options: any, that?: any) {
  return await unirequest(url, options, that)
}
