import account from './model/account'
import memberinfo from './model/memberinfo'
import ocenagentprofitcfg from './ocen/ocenagentprofitcfg'
import ocenfeigncollectcard from './ocen/ocenfeigncollectcard'
import ocenfeigncollectcardseries from './ocen/ocenfeigncollectcardseries'
import ocenfeignhomeadvert from './ocen/ocenfeignhomeadvert'
import ocenfeignmerchantinfo from './ocen/ocenfeignmerchantinfo'
import ocenfeignmerchantstatistics from './ocen/ocenfeignmerchantstatistics'
import ocenfeignquestionnaire from './ocen/ocenfeignquestionnaire'
import ocenfeignquestionnaireissue from './ocen/ocenfeignquestionnaireissue'
import ocenfeignstandardactivity from './ocen/ocenfeignstandardactivity'
import ocenfeignstandardactivityrule from './ocen/ocenfeignstandardactivityrule'
import ocenfeignstorefaq from './ocen/ocenfeignstorefaq'
import ocenfeignuseranswer from './ocen/ocenfeignuseranswer'
import ocenfeignusercollectcard from './ocen/ocenfeignusercollectcard'
import ocenhomeadvert from './ocen/ocenhomeadvert'
import oceninvitationcode from './ocen/oceninvitationcode'
import ocenmerchantaccount from './ocen/ocenmerchantaccount'
import ocenmerchantmedia from './ocen/ocenmerchantmedia'
import ocenmerchantmenu from './ocen/ocenmerchantmenu'
import ocenmerchantrole from './ocen/ocenmerchantrole'
import ocenstore from './ocen/ocenstore'
import ucenapishoppingaddress from './ucen/ucenapishoppingaddress'
import ucenapiusershare from './ucen/ucenapiusershare'
import ucenscancode from './ucen/ucenscancode'

export default {
  ...account,
  ...memberinfo,
  ...ocenagentprofitcfg,
  ...ocenfeigncollectcard,
  ...ocenfeigncollectcardseries,
  ...ocenfeignhomeadvert,
  ...ocenfeignmerchantinfo,
  ...ocenfeignmerchantstatistics,
  ...ocenfeignquestionnaire,
  ...ocenfeignquestionnaireissue,
  ...ocenfeignstandardactivity,
  ...ocenfeignstandardactivityrule,
  ...ocenfeignstorefaq,
  ...ocenfeignuseranswer,
  ...ocenfeignusercollectcard,
  ...ocenhomeadvert,
  ...oceninvitationcode,
  ...ocenmerchantaccount,
  ...ocenmerchantmedia,
  ...ocenmerchantmenu,
  ...ocenmerchantrole,
  ...ocenstore,
  ...ucenapishoppingaddress,
  ...ucenapiusershare,
  ...ucenscancode
}
