/**
 * 用户分享
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-19 17:47:21
 */
declare namespace _ucenapiusershare {
  interface UserShareCenDTO extends _default.post {
    /**
     * 活动代码
     */
    activityCode: string,
    /**
     * 被分享人id
     */
    byShareId: string,
    /**
     * 分享人id
     */
    shareId: string
  }
  interface ResultBeanstring extends _default.res {
    /**
     * 
     */
    data: string
  }
  /**
   * 分享 post
   */
  interface _papiusershareshare extends _ucenapiusershare.UserShareCenDTO {
  }
  /**
   * 分享 res
   */
  interface _rapiusershareshare extends _ucenapiusershare.ResultBeanstring {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 分享
     */
    _ucenusershareshare({ data, that }: { data: _ucenapiusershare._papiusershareshare, that?: any }): _ucenapiusershare._rapiusershareshare
  }
}
