/**
 * 用户
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-19 17:47:21
 */
declare namespace _ucenapishoppingaddress {
  interface AddShoppingAddressCenDTO extends _default.post {
    /**
     * 地址详情
     */
    address: string,
    /**
     * 市
     */
    city: string,
    /**
     * 是否默认
     */
    isDefault: boolean,
    /**
     * 收件人姓名
     */
    name: string,
    /**
     * 收件人电话
     */
    phone: string,
    /**
     * 省
     */
    province: string,
    /**
     * 区
     */
    region: string
  }
  interface ResultBeanlong extends _default.res {
    /**
     * 
     */
    data: number
  }
  interface ResultBeanboolean extends _default.res {
    /**
     * 
     */
    data: boolean
  }
  interface EditShoppingAddressCenDTO extends _default.post {
    /**
     * 地址详情
     */
    address: string,
    /**
     * 市
     */
    city: string,
    /**
     * id
     */
    id: number,
    /**
     * 是否默认
     */
    isDefault: boolean,
    /**
     * 收件人姓名
     */
    name: string,
    /**
     * 收件人电话
     */
    phone: string,
    /**
     * 省
     */
    province: string,
    /**
     * 区
     */
    region: string
  }
  interface ResultBeanGetAddressInfoVO extends _default.res {
    /**
     * 
     */
    data: _ucenapishoppingaddress.GetAddressInfoVO
  }
  interface GetAddressInfoVO {
    /**
     * 地址详情
     */
    address: string,
    /**
     * 市
     */
    city: string,
    /**
     * id
     */
    id: string,
    /**
     * 是否默认
     */
    isDefault: boolean,
    /**
     * 收件人姓名
     */
    name: string,
    /**
     * 收件人电话
     */
    phone: string,
    /**
     * 省
     */
    province: string,
    /**
     * 区
     */
    region: string
  }
  interface ResultBeanListGetAddressInfoVO extends _default.res {
    /**
     * 
     */
    data: _ucenapishoppingaddress.GetAddressInfoVO[]
  }
  interface ApiEditUserInfoDTO extends _default.post {
    /**
     * 头像
     */
    head: string,
    /**
     * 昵称
     */
    nickName: string
  }
  interface ResultBeanVoid extends _default.res {
  }
  interface ResultBeanApiUserInfoVO extends _default.res {
    /**
     * 
     */
    data: _ucenapishoppingaddress.ApiUserInfoVO
  }
  interface ApiUserInfoVO {
    /**
     * 生日
     */
    birthday: string,
    /**
     * 城市
     */
    city: string,
    /**
     * 国家
     */
    country: string,
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * 性别
     */
    gender: number,
    /**
     * 头像
     */
    head: string,
    /**
     * 用户标识
     */
    id: string,
    /**
     * 积分
     */
    integral: number,
    /**
     * 名称
     */
    name: string,
    /**
     * 推荐人（用户标识）
     */
    referrer: string
  }
  interface ApiUserBatchInfoDTO extends _default.post {
    /**
     * 用户标识集合
     */
    ids: string[]
  }
  interface ResultBeanListApiUserInfoVO extends _default.res {
    /**
     * 
     */
    data: _ucenapishoppingaddress.ApiUserInfoVO[]
  }
  interface WxLoginCliDTO extends _default.post {
    /**
     * 微信登录 code
     */
    jsCode: string,
    /**
     * 商家id
     */
    merchantId: number,
    /**
     * 店铺id
     */
    storeId: number
  }
  interface ResultBeanWxLoginCliVO extends _default.res {
    /**
     * 
     */
    data: _ucenapishoppingaddress.WxLoginCliVO
  }
  interface WxLoginCliVO {
    /**
     * Token
     */
    token: string
  }
  interface WxRegisterCliDTO extends _default.post {
    /**
     * 城市
     */
    city: string,
    /**
     * 国家
     */
    country: string,
    /**
     * 微信加密数据
     */
    encryptedData: string,
    /**
     * 性别
     */
    gender: number,
    /**
     * 头像
     */
    head: string,
    /**
     * 微信初始向量
     */
    iv: string,
    /**
     * 登录时获取的 code
     */
    jsCode: string,
    /**
     * 商家标识
     */
    merchantId: number,
    /**
     * 昵称
     */
    nickName: string,
    /**
     * 推荐会员标识
     */
    referrerMemberId: string,
    /**
     * 店铺id
     */
    storeId: number
  }
  interface ResultBeanWxRegisterCliVO extends _default.res {
    /**
     * 
     */
    data: _ucenapishoppingaddress.WxRegisterCliVO
  }
  interface WxRegisterCliVO {
    /**
     * Token
     */
    token: string
  }
  /**
   * 新增收货地址 post
   */
  interface _papishoppingaddressadd extends _ucenapishoppingaddress.AddShoppingAddressCenDTO {
  }
  /**
   * 新增收货地址 res
   */
  interface _rapishoppingaddressadd extends _ucenapishoppingaddress.ResultBeanlong {
  }
  /**
   * 删除地址 get
   */
  interface _papishoppingaddressdelid {
  }
  /**
   * 删除地址 res
   */
  interface _rapishoppingaddressdelid extends _ucenapishoppingaddress.ResultBeanboolean {
  }
  /**
   * 修改收货地址 post
   */
  interface _papishoppingaddressedit extends _ucenapishoppingaddress.EditShoppingAddressCenDTO {
  }
  /**
   * 修改收货地址 res
   */
  interface _rapishoppingaddressedit extends _ucenapishoppingaddress.ResultBeanboolean {
  }
  /**
   * 查询最新一条数据 get
   */
  interface _papishoppingaddressgetAddressInfo {
  }
  /**
   * 查询最新一条数据 res
   */
  interface _rapishoppingaddressgetAddressInfo extends _ucenapishoppingaddress.ResultBeanGetAddressInfoVO {
  }
  /**
   * 查询所有地址 get
   */
  interface _papishoppingaddressgetAll {
  }
  /**
   * 查询所有地址 res
   */
  interface _rapishoppingaddressgetAll extends _ucenapishoppingaddress.ResultBeanListGetAddressInfoVO {
  }
  /**
   * 更新用户信息 post
   */
  interface _papishoppingaddressedit extends _ucenapishoppingaddress.ApiEditUserInfoDTO {
  }
  /**
   * 更新用户信息 res
   */
  interface _rapishoppingaddressedit extends _ucenapishoppingaddress.ResultBeanVoid {
  }
  /**
   * 用户信息 get
   */
  interface _papishoppingaddressuserInfo {
  }
  /**
   * 用户信息 res
   */
  interface _rapishoppingaddressuserInfo extends _ucenapishoppingaddress.ResultBeanApiUserInfoVO {
  }
  /**
   * 用户信息（批量） post
   */
  interface _papishoppingaddressbatchInfo extends _ucenapishoppingaddress.ApiUserBatchInfoDTO {
  }
  /**
   * 用户信息（批量） res
   */
  interface _rapishoppingaddressbatchInfo extends _ucenapishoppingaddress.ResultBeanListApiUserInfoVO {
  }
  /**
   * 用户信息 get
   */
  interface _papishoppingaddressinfoid {
  }
  /**
   * 用户信息 res
   */
  interface _rapishoppingaddressinfoid extends _ucenapishoppingaddress.ResultBeanApiUserInfoVO {
  }
  /**
   * 微信登录 post
   */
  interface _papishoppingaddresswxLogin extends _ucenapishoppingaddress.WxLoginCliDTO {
  }
  /**
   * 微信登录 res
   */
  interface _rapishoppingaddresswxLogin extends _ucenapishoppingaddress.ResultBeanWxLoginCliVO {
  }
  /**
   * 微信注册 post
   */
  interface _papishoppingaddresswxRegister extends _ucenapishoppingaddress.WxRegisterCliDTO {
  }
  /**
   * 微信注册 res
   */
  interface _rapishoppingaddresswxRegister extends _ucenapishoppingaddress.ResultBeanWxRegisterCliVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 新增收货地址
     */
    _ucenshoppingAddressadd({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddressadd, that?: any }): _ucenapishoppingaddress._rapishoppingaddressadd,
    /**
     * 删除地址
     */
    _ucendelid({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddressdelid, that?: any }): _ucenapishoppingaddress._rapishoppingaddressdelid,
    /**
     * 修改收货地址
     */
    _ucenshoppingAddressedit({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddressedit, that?: any }): _ucenapishoppingaddress._rapishoppingaddressedit,
    /**
     * 查询最新一条数据
     */
    _ucenshoppingAddressgetAddressInfo({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddressgetAddressInfo, that?: any }): _ucenapishoppingaddress._rapishoppingaddressgetAddressInfo,
    /**
     * 查询所有地址
     */
    _ucenshoppingAddressgetAll({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddressgetAll, that?: any }): _ucenapishoppingaddress._rapishoppingaddressgetAll,
    /**
     * 更新用户信息
     */
    _uceninfoedit({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddressedit, that?: any }): _ucenapishoppingaddress._rapishoppingaddressedit,
    /**
     * 用户信息
     */
    _ucenuseruserInfo({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddressuserInfo, that?: any }): _ucenapishoppingaddress._rapishoppingaddressuserInfo,
    /**
     * 用户信息（批量）
     */
    _ucenappbatchInfo({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddressbatchInfo, that?: any }): _ucenapishoppingaddress._rapishoppingaddressbatchInfo,
    /**
     * 用户信息
     */
    _uceninfoid({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddressinfoid, that?: any }): _ucenapishoppingaddress._rapishoppingaddressinfoid,
    /**
     * 微信登录
     */
    _ucenvisitorwxLogin({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddresswxLogin, that?: any }): _ucenapishoppingaddress._rapishoppingaddresswxLogin,
    /**
     * 微信注册
     */
    _ucenvisitorwxRegister({ data, that }: { data: _ucenapishoppingaddress._papishoppingaddresswxRegister, that?: any }): _ucenapishoppingaddress._rapishoppingaddresswxRegister
  }
}
