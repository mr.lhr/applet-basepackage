export default {
  /**
   * post /user/api/shoppingAddress/add 新增收货地址
   */
  _ucenshoppingAddressadd: { method: 'post', url: '/user/api/shoppingAddress/add' },
  /**
   * get /user/api/shoppingAddress/del/{id} 删除地址
   */
  _ucendelid: { method: 'get', url: '/user/api/shoppingAddress/del/{id}' },
  /**
   * post /user/api/shoppingAddress/edit 修改收货地址
   */
  _ucenshoppingAddressedit: { method: 'post', url: '/user/api/shoppingAddress/edit' },
  /**
   * get /user/api/shoppingAddress/getAddressInfo 查询最新一条数据
   */
  _ucenshoppingAddressgetAddressInfo: { method: 'get', url: '/user/api/shoppingAddress/getAddressInfo' },
  /**
   * get /user/api/shoppingAddress/getAll 查询所有地址
   */
  _ucenshoppingAddressgetAll: { method: 'get', url: '/user/api/shoppingAddress/getAll' },
  /**
   * post /user/info/edit 更新用户信息
   */
  _uceninfoedit: { method: 'post', url: '/user/info/edit' },
  /**
   * get /user/userInfo 用户信息
   */
  _ucenuseruserInfo: { method: 'get', url: '/user/userInfo' },
  /**
   * post /user/visitor/app/batchInfo 用户信息（批量）
   */
  _ucenappbatchInfo: { method: 'post', url: '/user/visitor/app/batchInfo' },
  /**
   * get /user/visitor/app/info/{id} 用户信息
   */
  _uceninfoid: { method: 'get', url: '/user/visitor/app/info/{id}' },
  /**
   * post /user/visitor/wxLogin 微信登录
   */
  _ucenvisitorwxLogin: { method: 'post', url: '/user/visitor/wxLogin' },
  /**
   * post /user/visitor/wxRegister 微信注册
   */
  _ucenvisitorwxRegister: { method: 'post', url: '/user/visitor/wxRegister' }
}
