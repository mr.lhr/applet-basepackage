/**
 * 用户扫码
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-19 17:47:21
 */
declare namespace _ucenscancode {
  interface ScanCodeDTO extends _default.post {
    /**
     * 中文详细地址
     */
    address: string,
    /**
     * 市
     */
    city: string,
    /**
     * 标识码
     */
    code: string,
    /**
     * 纬度
     */
    latitude: string,
    /**
     * 经度
     */
    longitude: string,
    /**
     * 商家id
     */
    merchantId: number,
    /**
     * 省
     */
    province: string,
    /**
     * 店铺id
     */
    storeId: number,
    /**
     * 
     */
    vcode: string
  }
  interface ResultBeanScanCodeData extends _default.res {
    /**
     * 
     */
    data: _ucenscancode.ScanCodeData
  }
  interface ScanCodeData {
    /**
     * 奖品名称
     */
    awardsName: string,
    /**
     * 时间
     */
    createTime: string,
    /**
     * 是否首次中奖
     */
    isFirstWin: number,
    /**
     * 奖品种类
     */
    prizeCategory: number,
    /**
     * 奖品值
     */
    prizeValue: string
  }
  /**
   * 扫一扫 post
   */
  interface _plottery extends _ucenscancode.ScanCodeDTO {
  }
  /**
   * 扫一扫 res
   */
  interface _rlottery extends _ucenscancode.ResultBeanScanCodeData {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 扫一扫
     */
    _ucenscanCodelottery({ data, that }: { data: _ucenscancode._plottery, that?: any }): _ucenscancode._rlottery
  }
}
