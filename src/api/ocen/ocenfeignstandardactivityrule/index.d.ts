/**
 * 标准活动规则
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignstandardactivityrule {
  interface AddStandardActivityRuleCenDTO extends _default.post {
    /**
     * 活动代码
     */
    activityCode: string,
    /**
     * 规则json字符串
     */
    jsonStr: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface EditStandardActivityRuleCenDTO extends _default.post {
    /**
     * 活动代码
     */
    activityCode: string,
    /**
     * 规则json字符串
     */
    jsonStr: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface GetStandardActivityRuleCenDTO extends _default.post {
    /**
     * 活动代码
     */
    activityCode: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  /**
   * 配置活动规则 post
   */
  interface _pfeignstandardactivityruleadd extends _ocenfeignstandardactivityrule.AddStandardActivityRuleCenDTO {
  }
  /**
   * 配置活动规则 res
   */
  interface _rfeignstandardactivityruleadd {
  }
  /**
   * 编辑活动规则 post
   */
  interface _pfeignstandardactivityruleedit extends _ocenfeignstandardactivityrule.EditStandardActivityRuleCenDTO {
  }
  /**
   * 编辑活动规则 res
   */
  interface _rfeignstandardactivityruleedit {
  }
  /**
   * 查询活动规则 post
   */
  interface _pfeignstandardactivityruleget extends _ocenfeignstandardactivityrule.GetStandardActivityRuleCenDTO {
  }
  /**
   * 查询活动规则 res
   */
  interface _rfeignstandardactivityruleget {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 配置活动规则
     */
    _ocenstandardactivityruleadd({ data, that }: { data: _ocenfeignstandardactivityrule._pfeignstandardactivityruleadd, that?: any }): _ocenfeignstandardactivityrule._rfeignstandardactivityruleadd,
    /**
     * 编辑活动规则
     */
    _ocenstandardactivityruleedit({ data, that }: { data: _ocenfeignstandardactivityrule._pfeignstandardactivityruleedit, that?: any }): _ocenfeignstandardactivityrule._rfeignstandardactivityruleedit,
    /**
     * 查询活动规则
     */
    _ocenstandardactivityruleget({ data, that }: { data: _ocenfeignstandardactivityrule._pfeignstandardactivityruleget, that?: any }): _ocenfeignstandardactivityrule._rfeignstandardactivityruleget
  }
}
