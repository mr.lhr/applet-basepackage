export default {
  /**
   * post /feign/merchant/standardactivityrule/add 配置活动规则
   */
  _ocenstandardactivityruleadd: { method: 'post', url: '/feign/merchant/standardactivityrule/add' },
  /**
   * post /feign/merchant/standardactivityrule/edit 编辑活动规则
   */
  _ocenstandardactivityruleedit: { method: 'post', url: '/feign/merchant/standardactivityrule/edit' },
  /**
   * post /feign/merchant/standardactivityrule/get 查询活动规则
   */
  _ocenstandardactivityruleget: { method: 'post', url: '/feign/merchant/standardactivityrule/get' }
}
