/**
 * 常见问题
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignstorefaq {
  interface AddOrUpdateStoreFAQCenDTO extends _default.post {
    /**
     * 内容
     */
    content: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface GetStoreFAQCenDTO extends _default.post {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface StoreFAQCenVO {
    /**
     * 内容
     */
    content: string,
    /**
     * 创建时间
     */
    createTime: string
  }
  /**
   * 添加或编辑常见问题 post
   */
  interface _pfeignstorefaqaddOrUpdate extends _ocenfeignstorefaq.AddOrUpdateStoreFAQCenDTO {
  }
  /**
   * 添加或编辑常见问题 res
   */
  interface _rfeignstorefaqaddOrUpdate {
  }
  /**
   * 常见问题内容 post
   */
  interface _pfeignstorefaqget extends _ocenfeignstorefaq.GetStoreFAQCenDTO {
  }
  /**
   * 常见问题内容 res
   */
  interface _rfeignstorefaqget extends _ocenfeignstorefaq.StoreFAQCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 添加或编辑常见问题
     */
    _ocenstorefaqaddOrUpdate({ data, that }: { data: _ocenfeignstorefaq._pfeignstorefaqaddOrUpdate, that?: any }): _ocenfeignstorefaq._rfeignstorefaqaddOrUpdate,
    /**
     * 常见问题内容
     */
    _ocenstorefaqget({ data, that }: { data: _ocenfeignstorefaq._pfeignstorefaqget, that?: any }): _ocenfeignstorefaq._rfeignstorefaqget
  }
}
