export default {
  /**
   * post /feign/merchant/storefaq/addOrUpdate 添加或编辑常见问题
   */
  _ocenstorefaqaddOrUpdate: { method: 'post', url: '/feign/merchant/storefaq/addOrUpdate' },
  /**
   * post /feign/merchant/storefaq/get 常见问题内容
   */
  _ocenstorefaqget: { method: 'post', url: '/feign/merchant/storefaq/get' }
}
