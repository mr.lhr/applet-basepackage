export default {
  /**
   * post /merchantCenter/store/add 添加店铺
   */
  _ocenstoreadd: { method: 'post', url: '/merchantCenter/store/add' },
  /**
   * post /merchantCenter/store/edit 编辑店铺
   */
  _ocenstoreedit: { method: 'post', url: '/merchantCenter/store/edit' },
  /**
   * get /merchantCenter/store/queryByOrgId 通过orgId查询店铺
   */
  _ocenstorequeryByOrgId: { method: 'get', url: '/merchantCenter/store/queryByOrgId' },
  /**
   * get /merchantCenter/storeFaq/queryByStoreId 通过storeid查询店铺常见问题详情
   */
  _ocenstoreFaqqueryByStoreId: { method: 'get', url: '/merchantCenter/storeFaq/queryByStoreId' }
}
