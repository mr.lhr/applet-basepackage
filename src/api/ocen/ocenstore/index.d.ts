/**
 * 店铺
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenstore {
  interface AddStoreCenDTO extends _default.post {
    /**
     * 商家标识
     */
    merchantId: number,
    /**
     * 店铺名称
     */
    name: string,
    /**
     * 组织id
     */
    orgId: number
  }
  interface EditStoreCenDTO extends _default.post {
    /**
     * 店铺id
     */
    id: number,
    /**
     * 商家标识
     */
    merchantId: number,
    /**
     * 组织id
     */
    orgId: number,
    /**
     * 店铺名称
     */
    storeName: string
  }
  interface StoreInfoVO {
    /**
     * 店铺id
     */
    id: number,
    /**
     * 商家标识
     */
    merchantId: number,
    /**
     * 组织ID
     */
    orgId: number,
    /**
     * 状态（正常：0，禁止：1）
     */
    status: number,
    /**
     * 店铺名称
     */
    storeName: string
  }
  interface StoreFAQCenVO {
    /**
     * 内容
     */
    content: string,
    /**
     * 创建时间
     */
    createTime: string
  }
  /**
   * 添加店铺 post
   */
  interface _padd extends _ocenstore.AddStoreCenDTO {
  }
  /**
   * 添加店铺 res
   */
  interface _radd {
  }
  /**
   * 编辑店铺 post
   */
  interface _pedit extends _ocenstore.EditStoreCenDTO {
  }
  /**
   * 编辑店铺 res
   */
  interface _redit {
  }
  /**
   * 通过orgId查询店铺 get
   */
  interface _pqueryByOrgId {
  }
  /**
   * 通过orgId查询店铺 res
   */
  interface _rqueryByOrgId extends _ocenstore.StoreInfoVO {
  }
  /**
   * 通过storeid查询店铺常见问题详情 get
   */
  interface _pstorequeryByStoreId {
  }
  /**
   * 通过storeid查询店铺常见问题详情 res
   */
  interface _rstorequeryByStoreId extends _ocenstore.StoreFAQCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 添加店铺
     */
    _ocenstoreadd({ data, that }: { data: _ocenstore._padd, that?: any }): _ocenstore._radd,
    /**
     * 编辑店铺
     */
    _ocenstoreedit({ data, that }: { data: _ocenstore._pedit, that?: any }): _ocenstore._redit,
    /**
     * 通过orgId查询店铺
     */
    _ocenstorequeryByOrgId({ data, that }: { data: _ocenstore._pqueryByOrgId, that?: any }): _ocenstore._rqueryByOrgId,
    /**
     * 通过storeid查询店铺常见问题详情
     */
    _ocenstoreFaqqueryByStoreId({ data, that }: { data: _ocenstore._pstorequeryByStoreId, that?: any }): _ocenstore._rstorequeryByStoreId
  }
}
