export default {
  /**
   * post /feign/merchant/standardactivity/add 新增标准活动
   */
  _ocenstandardactivityadd: { method: 'post', url: '/feign/merchant/standardactivity/add' },
  /**
   * post /feign/merchant/standardactivity/edit 编辑标准活动
   */
  _ocenstandardactivityedit: { method: 'post', url: '/feign/merchant/standardactivity/edit' },
  /**
   * post /feign/merchant/standardactivity/query 查询标准活动
   */
  _ocenstandardactivityquery: { method: 'post', url: '/feign/merchant/standardactivity/query' }
}
