/**
 * 标准活动
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignstandardactivity {
  interface AddStandardActivityCenDTO extends _default.post {
    /**
     * 活动代码
     */
    activityCode: string,
    /**
     * 描述
     */
    description: string,
    /**
     * 活动名称
     */
    name: string
  }
  interface EditStandardActivityCenDTO extends _default.post {
    /**
     * 活动代码
     */
    code: string,
    /**
     * 描述
     */
    description: string,
    /**
     * id
     */
    id: string,
    /**
     * 活动名称
     */
    name: string
  }
  interface PageDataStandardActivityCenVO {
    /**
     * 数据集合
     */
    rows: _ocenfeignstandardactivity.StandardActivityCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  interface StandardActivityCenVO {
    /**
     * 活动代码
     */
    activityCode: string,
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * 活动描述
     */
    description: string,
    /**
     * id
     */
    id: string,
    /**
     * 活动名称
     */
    name: string
  }
  /**
   * 新增标准活动 post
   */
  interface _pfeignstandardactivityadd extends _ocenfeignstandardactivity.AddStandardActivityCenDTO {
  }
  /**
   * 新增标准活动 res
   */
  interface _rfeignstandardactivityadd {
  }
  /**
   * 编辑标准活动 post
   */
  interface _pfeignstandardactivityedit extends _ocenfeignstandardactivity.EditStandardActivityCenDTO {
  }
  /**
   * 编辑标准活动 res
   */
  interface _rfeignstandardactivityedit {
  }
  /**
   * 查询标准活动 post
   */
  interface _pfeignstandardactivityquery {
  }
  /**
   * 查询标准活动 res
   */
  interface _rfeignstandardactivityquery extends _ocenfeignstandardactivity.PageDataStandardActivityCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 新增标准活动
     */
    _ocenstandardactivityadd({ data, that }: { data: _ocenfeignstandardactivity._pfeignstandardactivityadd, that?: any }): _ocenfeignstandardactivity._rfeignstandardactivityadd,
    /**
     * 编辑标准活动
     */
    _ocenstandardactivityedit({ data, that }: { data: _ocenfeignstandardactivity._pfeignstandardactivityedit, that?: any }): _ocenfeignstandardactivity._rfeignstandardactivityedit,
    /**
     * 查询标准活动
     */
    _ocenstandardactivityquery({ data, that }: { data: _ocenfeignstandardactivity._pfeignstandardactivityquery, that?: any }): _ocenfeignstandardactivity._rfeignstandardactivityquery
  }
}
