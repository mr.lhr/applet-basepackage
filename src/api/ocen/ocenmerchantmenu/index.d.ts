/**
 * 商家菜单
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenmerchantmenu {
  interface AddMenuCenDTO extends _default.post {
    /**
     * 菜单图标
     */
    icon: string,
    /**
     * 是否父级菜单
     */
    isParent: number,
    /**
     * 是否显示
     */
    isShow: number,
    /**
     * 菜单名称
     */
    name: string,
    /**
     * 父级ID
     */
    parentId: string,
    /**
     * 排序
     */
    showIndex: number
  }
  interface ResultBeanstring extends _default.res {
    /**
     * 
     */
    data: string
  }
  interface DelMenuCenDTO extends _default.post {
    /**
     * 菜单ID
     */
    id: string
  }
  interface EditMenuCenDTO extends _default.post {
    /**
     * 菜单图标
     */
    icon: string,
    /**
     * 菜单ID
     */
    id: string,
    /**
     * 是否父级菜单
     */
    isParent: number,
    /**
     * 是否显示
     */
    isShow: number,
    /**
     * 菜单名称
     */
    name: string,
    /**
     * 父级ID
     */
    parentId: string,
    /**
     * 排序
     */
    showIndex: number
  }
  interface QueryMenuInfoDTO extends _default.listPagePost {
    /**
     * 菜单ID
     */
    id: string
  }
  interface ResultBeanQueryMenuInfoVO extends _default.res {
    /**
     * 
     */
    data: _ocenmerchantmenu.QueryMenuInfoVO
  }
  interface QueryMenuInfoVO {
    /**
     * 图标
     */
    icon: string,
    /**
     * 菜单ID
     */
    id: string,
    /**
     * 是否父级
     */
    isParent: number,
    /**
     * 是否显示
     */
    isShow: number,
    /**
     * 菜单名称
     */
    name: string,
    /**
     * 父级标识串
     */
    parentJson: string,
    /**
     * 排序
     */
    showIndex: number
  }
  interface QueryMenuListCenDTO extends _default.listPagePost {
    /**
     * 父级ID
     */
    parentId: string
  }
  interface ResultBeanListQueryMenuListCenVO extends _default.res {
    /**
     * 
     */
    data: _ocenmerchantmenu.QueryMenuListCenVO[]
  }
  interface QueryMenuListCenVO {
    /**
     * 菜单图标
     */
    icon: string,
    /**
     * 菜单ID
     */
    id: string,
    /**
     * 是否显示
     */
    isShow: number,
    /**
     * 菜单名称
     */
    name: string,
    /**
     * 父级ID
     */
    parentId: string,
    /**
     * 菜单排序
     */
    showIndex: number
  }
  /**
   * 添加菜单 post
   */
  interface _paddMenu extends _ocenmerchantmenu.AddMenuCenDTO {
  }
  /**
   * 添加菜单 res
   */
  interface _raddMenu extends _ocenmerchantmenu.ResultBeanstring {
  }
  /**
   * 删除菜单 post
   */
  interface _pdelMenu extends _ocenmerchantmenu.DelMenuCenDTO {
  }
  /**
   * 删除菜单 res
   */
  interface _rdelMenu extends _ocenmerchantmenu.ResultBeanstring {
  }
  /**
   * 编辑菜单 post
   */
  interface _peditMenu extends _ocenmerchantmenu.EditMenuCenDTO {
  }
  /**
   * 编辑菜单 res
   */
  interface _reditMenu extends _ocenmerchantmenu.ResultBeanstring {
  }
  /**
   * 查询菜单信息 post
   */
  interface _pqueryMenuInfo extends _ocenmerchantmenu.QueryMenuInfoDTO {
  }
  /**
   * 查询菜单信息 res
   */
  interface _rqueryMenuInfo extends _ocenmerchantmenu.ResultBeanQueryMenuInfoVO {
  }
  /**
   * 查询菜单列表 post
   */
  interface _pqueryMenuList extends _ocenmerchantmenu.QueryMenuListCenDTO {
  }
  /**
   * 查询菜单列表 res
   */
  interface _rqueryMenuList extends _ocenmerchantmenu.ResultBeanListQueryMenuListCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 添加菜单
     */
    _ocenmerchantMenuaddMenu({ data, that }: { data: _ocenmerchantmenu._paddMenu, that?: any }): _ocenmerchantmenu._raddMenu,
    /**
     * 删除菜单
     */
    _ocenmerchantMenudelMenu({ data, that }: { data: _ocenmerchantmenu._pdelMenu, that?: any }): _ocenmerchantmenu._rdelMenu,
    /**
     * 编辑菜单
     */
    _ocenmerchantMenueditMenu({ data, that }: { data: _ocenmerchantmenu._peditMenu, that?: any }): _ocenmerchantmenu._reditMenu,
    /**
     * 查询菜单信息
     */
    _ocenmerchantMenuqueryMenuInfo({ data, that }: { data: _ocenmerchantmenu._pqueryMenuInfo, that?: any }): _ocenmerchantmenu._rqueryMenuInfo,
    /**
     * 查询菜单列表
     */
    _ocenmerchantMenuqueryMenuList({ data, that }: { data: _ocenmerchantmenu._pqueryMenuList, that?: any }): _ocenmerchantmenu._rqueryMenuList
  }
}
