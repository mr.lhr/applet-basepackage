export default {
  /**
   * post /merchantCenter/merchantMenu/addMenu 添加菜单
   */
  _ocenmerchantMenuaddMenu: { method: 'post', url: '/merchantCenter/merchantMenu/addMenu' },
  /**
   * post /merchantCenter/merchantMenu/delMenu 删除菜单
   */
  _ocenmerchantMenudelMenu: { method: 'post', url: '/merchantCenter/merchantMenu/delMenu' },
  /**
   * post /merchantCenter/merchantMenu/editMenu 编辑菜单
   */
  _ocenmerchantMenueditMenu: { method: 'post', url: '/merchantCenter/merchantMenu/editMenu' },
  /**
   * post /merchantCenter/merchantMenu/queryMenuInfo 查询菜单信息
   */
  _ocenmerchantMenuqueryMenuInfo: { method: 'post', url: '/merchantCenter/merchantMenu/queryMenuInfo' },
  /**
   * post /merchantCenter/merchantMenu/queryMenuList 查询菜单列表
   */
  _ocenmerchantMenuqueryMenuList: { method: 'post', url: '/merchantCenter/merchantMenu/queryMenuList' }
}
