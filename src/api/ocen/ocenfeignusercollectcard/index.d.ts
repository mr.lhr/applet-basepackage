/**
 * 用户收集卡
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignusercollectcard {
  interface QueryUserCollectCardCenDTO extends _default.listPagePost {
    /**
     * 卡名称
     */
    cardName: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 系列id
     */
    seriesId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface PageDataUserCollectCardCenVO {
    /**
     * 数据集合
     */
    rows: _ocenfeignusercollectcard.UserCollectCardCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  interface UserCollectCardCenVO {
    /**
     * 收集卡ID
     */
    cardId: string,
    /**
     * 数量
     */
    count: number,
    /**
     * ID
     */
    id: string,
    /**
     * 商家ID
     */
    merchantId: string,
    /**
     * 系列ID
     */
    seriesId: string,
    /**
     * 商家ID
     */
    storeId: string,
    /**
     * 更新时间
     */
    updateTime: string,
    /**
     * 用户ID
     */
    userId: string
  }
  /**
   * 查询用户收集卡 post
   */
  interface _pfeignusercollectcardquery extends _ocenfeignusercollectcard.QueryUserCollectCardCenDTO {
  }
  /**
   * 查询用户收集卡 res
   */
  interface _rfeignusercollectcardquery extends _ocenfeignusercollectcard.PageDataUserCollectCardCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 查询用户收集卡
     */
    _ocenusercollectcardquery({ data, that }: { data: _ocenfeignusercollectcard._pfeignusercollectcardquery, that?: any }): _ocenfeignusercollectcard._rfeignusercollectcardquery
  }
}
