export default {
  /**
   * post /feign/merchant/usercollectcard/query 查询用户收集卡
   */
  _ocenusercollectcardquery: { method: 'post', url: '/feign/merchant/usercollectcard/query' }
}
