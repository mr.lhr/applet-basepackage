/**
 * 问题
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignquestionnaireissue {
  interface AddQuestionnaireIssueCenDTO extends _default.post {
    /**
     * 问题
     */
    issue: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 选项
     */
    option: string[],
    /**
     * 问卷id
     */
    questionnaireId: string,
    /**
     * 店铺id
     */
    storeId: string,
    /**
     * 题型
     */
    type: string
  }
  interface DeleteQuestionnaireIssueCenDTO extends _default.post {
    /**
     * 问卷id
     */
    issueId: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface EditQuestionnaireIssueCenDTO extends _default.post {
    /**
     * id
     */
    id: string,
    /**
     * 问题
     */
    issue: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 题号
     */
    number: string,
    /**
     * 选项
     */
    option: string[],
    /**
     * 店铺id
     */
    storeId: string,
    /**
     * 题型
     */
    type: string
  }
  interface GetQuestionnaireIssueCenDTO extends _default.post {
    /**
     * 页码
     */
    currentPage: number,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 问卷id
     */
    questionnaireId: string,
    /**
     * 页面大小
     */
    showCount: number,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface PageDataQuestionnaireIssueCenVO {
    /**
     * 数据集合
     */
    rows: _ocenfeignquestionnaireissue.QuestionnaireIssueCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  interface QuestionnaireIssueCenVO {
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * id
     */
    id: string,
    /**
     * 问题
     */
    issue: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 题号
     */
    number: string,
    /**
     * 选项
     */
    option: string[],
    /**
     * 店铺id
     */
    storeId: string,
    /**
     * 题型
     */
    type: string
  }
  /**
   * 新增问题 post
   */
  interface _pfeignquestionnaireissueadd extends _ocenfeignquestionnaireissue.AddQuestionnaireIssueCenDTO {
  }
  /**
   * 新增问题 res
   */
  interface _rfeignquestionnaireissueadd {
  }
  /**
   * delete post
   */
  interface _pfeignquestionnaireissuedelete extends _ocenfeignquestionnaireissue.DeleteQuestionnaireIssueCenDTO {
  }
  /**
   * delete res
   */
  interface _rfeignquestionnaireissuedelete {
  }
  /**
   * 编辑问题 post
   */
  interface _pfeignquestionnaireissueedit extends _ocenfeignquestionnaireissue.EditQuestionnaireIssueCenDTO {
  }
  /**
   * 编辑问题 res
   */
  interface _rfeignquestionnaireissueedit {
  }
  /**
   * 问题列表 post
   */
  interface _pfeignquestionnaireissuegetIssueList extends _ocenfeignquestionnaireissue.GetQuestionnaireIssueCenDTO {
  }
  /**
   * 问题列表 res
   */
  interface _rfeignquestionnaireissuegetIssueList extends _ocenfeignquestionnaireissue.PageDataQuestionnaireIssueCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 新增问题
     */
    _ocenquestionnaireissueadd({ data, that }: { data: _ocenfeignquestionnaireissue._pfeignquestionnaireissueadd, that?: any }): _ocenfeignquestionnaireissue._rfeignquestionnaireissueadd,
    /**
     * delete
     */
    _ocenquestionnaireissuedelete({ data, that }: { data: _ocenfeignquestionnaireissue._pfeignquestionnaireissuedelete, that?: any }): _ocenfeignquestionnaireissue._rfeignquestionnaireissuedelete,
    /**
     * 编辑问题
     */
    _ocenquestionnaireissueedit({ data, that }: { data: _ocenfeignquestionnaireissue._pfeignquestionnaireissueedit, that?: any }): _ocenfeignquestionnaireissue._rfeignquestionnaireissueedit,
    /**
     * 问题列表
     */
    _ocenquestionnaireissuegetIssueList({ data, that }: { data: _ocenfeignquestionnaireissue._pfeignquestionnaireissuegetIssueList, that?: any }): _ocenfeignquestionnaireissue._rfeignquestionnaireissuegetIssueList
  }
}
