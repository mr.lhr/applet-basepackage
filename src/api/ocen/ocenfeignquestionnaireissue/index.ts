export default {
  /**
   * post /feign/merchant/questionnaireissue/add 新增问题
   */
  _ocenquestionnaireissueadd: { method: 'post', url: '/feign/merchant/questionnaireissue/add' },
  /**
   * post /feign/merchant/questionnaireissue/delete delete
   */
  _ocenquestionnaireissuedelete: { method: 'post', url: '/feign/merchant/questionnaireissue/delete' },
  /**
   * post /feign/merchant/questionnaireissue/edit 编辑问题
   */
  _ocenquestionnaireissueedit: { method: 'post', url: '/feign/merchant/questionnaireissue/edit' },
  /**
   * post /feign/merchant/questionnaireissue/getIssueList 问题列表
   */
  _ocenquestionnaireissuegetIssueList: { method: 'post', url: '/feign/merchant/questionnaireissue/getIssueList' }
}
