export default {
  /**
   * post /feign/merchant/questionnaire/add 添加问卷
   */
  _ocenquestionnaireadd: { method: 'post', url: '/feign/merchant/questionnaire/add' },
  /**
   * post /feign/merchant/questionnaire/edit 编辑问卷
   */
  _ocenquestionnaireedit: { method: 'post', url: '/feign/merchant/questionnaire/edit' },
  /**
   * get /feign/merchant/questionnaire/get 问卷详情
   */
  _ocenquestionnaireget: { method: 'get', url: '/feign/merchant/questionnaire/get' },
  /**
   * get /feign/merchant/questionnaire/list 问卷列表
   */
  _ocenquestionnairelist: { method: 'get', url: '/feign/merchant/questionnaire/list' },
  /**
   * post /feign/merchant/questionnaire/query 查询问卷
   */
  _ocenquestionnairequery: { method: 'post', url: '/feign/merchant/questionnaire/query' }
}
