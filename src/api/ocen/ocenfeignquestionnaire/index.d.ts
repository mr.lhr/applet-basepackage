/**
 * 问卷
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignquestionnaire {
  interface AddQuestionnaireCenDTO extends _default.post {
    /**
     * 描述
     */
    description: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 问卷名称
     */
    name: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface EditQuestionnaireCenDTO extends _default.post {
    /**
     * 描述
     */
    description: string,
    /**
     * id
     */
    id: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 问卷名称
     */
    name: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface GetQuestionnaireCenDTO extends _default.post {
    /**
     * id
     */
    id: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface QuestionnaireCenVO {
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * 描述
     */
    description: string,
    /**
     * ID
     */
    id: string,
    /**
     * 商家ID
     */
    merchantId: string,
    /**
     * 问卷名称
     */
    name: string,
    /**
     * 商家ID
     */
    storeId: string
  }
  interface QuestionnaireListCenDTO extends _default.post {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface QueryQuestionnaireCenDTO extends _default.listPagePost {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 问卷名称
     */
    name: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface PageDataQuestionnaireCenVO {
    /**
     * 数据集合
     */
    rows: _ocenfeignquestionnaire.QuestionnaireCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  /**
   * 添加问卷 post
   */
  interface _pfeignquestionnaireadd extends _ocenfeignquestionnaire.AddQuestionnaireCenDTO {
  }
  /**
   * 添加问卷 res
   */
  interface _rfeignquestionnaireadd {
  }
  /**
   * 编辑问卷 post
   */
  interface _pfeignquestionnaireedit extends _ocenfeignquestionnaire.EditQuestionnaireCenDTO {
  }
  /**
   * 编辑问卷 res
   */
  interface _rfeignquestionnaireedit {
  }
  /**
   * 问卷详情 get
   */
  interface _pfeignquestionnaireget extends _ocenfeignquestionnaire.GetQuestionnaireCenDTO {
  }
  /**
   * 问卷详情 res
   */
  interface _rfeignquestionnaireget extends _ocenfeignquestionnaire.QuestionnaireCenVO {
  }
  /**
   * 问卷列表 get
   */
  interface _pfeignquestionnairelist extends _ocenfeignquestionnaire.QuestionnaireListCenDTO {
  }
  /**
   * 问卷列表 res
   */
  interface _rfeignquestionnairelist {
  }
  /**
   * 查询问卷 post
   */
  interface _pfeignquestionnairequery extends _ocenfeignquestionnaire.QueryQuestionnaireCenDTO {
  }
  /**
   * 查询问卷 res
   */
  interface _rfeignquestionnairequery extends _ocenfeignquestionnaire.PageDataQuestionnaireCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 添加问卷
     */
    _ocenquestionnaireadd({ data, that }: { data: _ocenfeignquestionnaire._pfeignquestionnaireadd, that?: any }): _ocenfeignquestionnaire._rfeignquestionnaireadd,
    /**
     * 编辑问卷
     */
    _ocenquestionnaireedit({ data, that }: { data: _ocenfeignquestionnaire._pfeignquestionnaireedit, that?: any }): _ocenfeignquestionnaire._rfeignquestionnaireedit,
    /**
     * 问卷详情
     */
    _ocenquestionnaireget({ data, that }: { data: _ocenfeignquestionnaire._pfeignquestionnaireget, that?: any }): _ocenfeignquestionnaire._rfeignquestionnaireget,
    /**
     * 问卷列表
     */
    _ocenquestionnairelist({ data, that }: { data: _ocenfeignquestionnaire._pfeignquestionnairelist, that?: any }): _ocenfeignquestionnaire._rfeignquestionnairelist,
    /**
     * 查询问卷
     */
    _ocenquestionnairequery({ data, that }: { data: _ocenfeignquestionnaire._pfeignquestionnairequery, that?: any }): _ocenfeignquestionnaire._rfeignquestionnairequery
  }
}
