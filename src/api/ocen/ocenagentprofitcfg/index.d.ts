/**
 * agent-profit-cfg-controller
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenagentprofitcfg {
  interface GetAgentProfitCfgCenDTO extends _default.post {
    /**
     * 商家ID集合
     */
    merchantIds: string[]
  }
  interface ResultBeanListListAgentProfitCfgCenVO extends _default.res {
    /**
     * 
     */
    data: _ocenagentprofitcfg.ListAgentProfitCfgCenVO[]
  }
  interface ListAgentProfitCfgCenVO {
    /**
     * 商家收益比例
     */
    merchantEarning: number,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 二级收益比列
     */
    partnerEarning: number,
    /**
     * 一级收益比列
     */
    shopkeeperEarning: number
  }
  interface ResultBeanPageDataListAgentProfitCfgCenVO extends _default.res {
    /**
     * 
     */
    data: _ocenagentprofitcfg.PageDataListAgentProfitCfgCenVO
  }
  interface PageDataListAgentProfitCfgCenVO {
    /**
     * 数据集合
     */
    rows: _ocenagentprofitcfg.ListAgentProfitCfgCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  /**
   * 查询收益信息 post
   */
  interface _pget extends _ocenagentprofitcfg.GetAgentProfitCfgCenDTO {
  }
  /**
   * 查询收益信息 res
   */
  interface _rget extends _ocenagentprofitcfg.ResultBeanListListAgentProfitCfgCenVO {
  }
  /**
   * 分页查询收益信息 post
   */
  interface _pgetPage {
  }
  /**
   * 分页查询收益信息 res
   */
  interface _rgetPage extends _ocenagentprofitcfg.ResultBeanPageDataListAgentProfitCfgCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 查询收益信息
     */
    _ocenagentProfitCfgget({ data, that }: { data: _ocenagentprofitcfg._pget, that?: any }): _ocenagentprofitcfg._rget,
    /**
     * 分页查询收益信息
     */
    _ocenagentProfitCfggetPage({ data, that }: { data: _ocenagentprofitcfg._pgetPage, that?: any }): _ocenagentprofitcfg._rgetPage
  }
}
