export default {
  /**
   * post /merchant_center/agentProfitCfg/get 查询收益信息
   */
  _ocenagentProfitCfgget: { method: 'post', url: '/merchant_center/agentProfitCfg/get' },
  /**
   * post /merchant_center/agentProfitCfg/getPage 分页查询收益信息
   */
  _ocenagentProfitCfggetPage: { method: 'post', url: '/merchant_center/agentProfitCfg/getPage' }
}
