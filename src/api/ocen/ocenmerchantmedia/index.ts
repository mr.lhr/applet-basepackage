export default {
  /**
   * post /merchantCenter/merchantMedia/add 新增商家媒体信息
   */
  _ocenmerchantMediaadd: { method: 'post', url: '/merchantCenter/merchantMedia/add' },
  /**
   * post /merchantCenter/merchantMedia/query 查询商家媒体信息
   */
  _ocenmerchantMediaquery: { method: 'post', url: '/merchantCenter/merchantMedia/query' },
  /**
   * post /merchantCenter/merchantMedia/update 修改商家媒体信息
   */
  _ocenmerchantMediaupdate: { method: 'post', url: '/merchantCenter/merchantMedia/update' }
}
