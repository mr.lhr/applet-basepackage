/**
 * 商家媒体
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenmerchantmedia {
  interface AddMerchantMediaCenDTO extends _default.post {
    /**
     * 云存储方式
     */
    cloudType: string,
    /**
     * 云存储地址
     */
    cloudUrl: string,
    /**
     * 媒体类型
     */
    mediaType: string,
    /**
     * 商家标识
     */
    merchantId: number,
    /**
     * 排序
     */
    sort: number,
    /**
     * 使用类型
     */
    useType: string
  }
  interface ResultBeanstring extends _default.res {
    /**
     * 
     */
    data: string
  }
  interface QueryMerchantMediaCenDTO extends _default.listPagePost {
    /**
     * 云存储方式
     */
    cloudType: string,
    /**
     * 媒体类型
     */
    mediaType: string,
    /**
     * 商家标识
     */
    merchantId: number,
    /**
     * 使用类型
     */
    useType: string
  }
  interface ResultBeanPageDataQueryMerchantMediaCenVO extends _default.res {
    /**
     * 
     */
    data: _ocenmerchantmedia.PageDataQueryMerchantMediaCenVO
  }
  interface PageDataQueryMerchantMediaCenVO {
    /**
     * 数据集合
     */
    rows: _ocenmerchantmedia.QueryMerchantMediaCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  interface QueryMerchantMediaCenVO {
    /**
     * 云存储地址
     */
    cloudUrl: string,
    /**
     * 商家媒体ID
     */
    id: string
  }
  interface UpdateMerchantMediaCenDTO extends _default.post {
    /**
     * 云存储地址
     */
    cloudUrl: string,
    /**
     * ID
     */
    id: number
  }
  /**
   * 新增商家媒体信息 post
   */
  interface _padd extends _ocenmerchantmedia.AddMerchantMediaCenDTO {
  }
  /**
   * 新增商家媒体信息 res
   */
  interface _radd extends _ocenmerchantmedia.ResultBeanstring {
  }
  /**
   * 查询商家媒体信息 post
   */
  interface _pquery extends _ocenmerchantmedia.QueryMerchantMediaCenDTO {
  }
  /**
   * 查询商家媒体信息 res
   */
  interface _rquery extends _ocenmerchantmedia.ResultBeanPageDataQueryMerchantMediaCenVO {
  }
  /**
   * 修改商家媒体信息 post
   */
  interface _pupdate extends _ocenmerchantmedia.UpdateMerchantMediaCenDTO {
  }
  /**
   * 修改商家媒体信息 res
   */
  interface _rupdate extends _ocenmerchantmedia.ResultBeanstring {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 新增商家媒体信息
     */
    _ocenmerchantMediaadd({ data, that }: { data: _ocenmerchantmedia._padd, that?: any }): _ocenmerchantmedia._radd,
    /**
     * 查询商家媒体信息
     */
    _ocenmerchantMediaquery({ data, that }: { data: _ocenmerchantmedia._pquery, that?: any }): _ocenmerchantmedia._rquery,
    /**
     * 修改商家媒体信息
     */
    _ocenmerchantMediaupdate({ data, that }: { data: _ocenmerchantmedia._pupdate, that?: any }): _ocenmerchantmedia._rupdate
  }
}
