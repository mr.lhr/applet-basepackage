/**
 * 轮播图
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenhomeadvert {
  interface HomeAdvertDTO extends _default.post {
    /**
     * 页码
     */
    currentPage: number,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 页面大小
     */
    showCount: number,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface ResultBeanPageDataHomeAdvertVO extends _default.res {
    /**
     * 
     */
    data: _ocenhomeadvert.PageDataHomeAdvertVO
  }
  interface PageDataHomeAdvertVO {
    /**
     * 数据集合
     */
    rows: _ocenhomeadvert.HomeAdvertVO[],
    /**
     * 总行数
     */
    total: number
  }
  interface HomeAdvertVO {
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * id
     */
    id: string,
    /**
     * 图片
     */
    image: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 跳转路径
     */
    skipUrl: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  /**
   * 轮播图分页列表 post
   */
  interface _ppage extends _ocenhomeadvert.HomeAdvertDTO {
  }
  /**
   * 轮播图分页列表 res
   */
  interface _rpage extends _ocenhomeadvert.ResultBeanPageDataHomeAdvertVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 轮播图分页列表
     */
    _ocenhomeadvertpage({ data, that }: { data: _ocenhomeadvert._ppage, that?: any }): _ocenhomeadvert._rpage
  }
}
