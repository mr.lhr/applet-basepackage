export default {
  /**
   * post /feign/merchant/merchantInfo/add 新增商家信息
   */
  _ocenmerchantInfoadd: { method: 'post', url: '/feign/merchant/merchantInfo/add' },
  /**
   * post /feign/merchant/merchantInfo/edit 编辑商家信息
   */
  _ocenmerchantInfoedit: { method: 'post', url: '/feign/merchant/merchantInfo/edit' },
  /**
   * post /feign/merchant/merchantInfo/get 查询商家信息
   */
  _ocenmerchantInfoget: { method: 'post', url: '/feign/merchant/merchantInfo/get' },
  /**
   * post /feign/merchant/merchantInfo/query 商家列表
   */
  _ocenmerchantInfoquery: { method: 'post', url: '/feign/merchant/merchantInfo/query' }
}
