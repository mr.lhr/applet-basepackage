/**
 * 商家信息
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignmerchantinfo {
  interface AddMerchantInfoCenDTO extends _default.post {
    /**
     * 地址
     */
    address: string,
    /**
     * 主营类目
     */
    businessCategoryCode: string,
    /**
     * 统一社会信用代码
     */
    businessLicenseCode: string,
    /**
     * 营业执照名称
     */
    businessLicenseName: string,
    /**
     * 营业执照URL
     */
    businessLicenseUrl: string,
    /**
     * 经营范围
     */
    businessScope: string,
    /**
     * 联系邮箱
     */
    contactEmail: string,
    /**
     * 联系手机号
     */
    contactPhone: string,
    /**
     * 联系人
     */
    contacts: string,
    /**
     * 商家描述
     */
    description: string,
    /**
     * 商家名称
     */
    name: string,
    /**
     * 组织id
     */
    orgId: number,
    /**
     * 客服电话
     */
    serviceTel: string,
    /**
     * 特殊资质证明URL
     */
    specialLicenseUrl: string,
    /**
     * 店铺名称
     */
    storeName: string,
    /**
     * 类型 个体经营：individual，企业经营：company
     */
    type: string
  }
  interface EditMerchantInfoCenDTO extends _default.post {
    /**
     * 封面
     */
    cover: string,
    /**
     * 商家简介
     */
    description: string,
    /**
     * 商家logo
     */
    logo: string,
    /**
     * 商家ID
     */
    merchantId: string,
    /**
     * 商家名称
     */
    name: string
  }
  interface GetMerchantCenDTO extends _default.post {
    /**
     * 商家id
     */
    merchantId: string
  }
  interface GetMerchantInfoCenVO {
    /**
     * 地址
     */
    address: string,
    /**
     * 主营类目
     */
    businessCategoryCode: string,
    /**
     * 统一社会信用代码
     */
    businessLicenseCode: string,
    /**
     * 营业执照名称
     */
    businessLicenseName: string,
    /**
     * 营业执照URL
     */
    businessLicenseUrl: string,
    /**
     * 经营范围
     */
    businessScope: string,
    /**
     * 联系邮箱
     */
    contactEmail: string,
    /**
     * 联系手机号
     */
    contactPhone: string,
    /**
     * 联系人
     */
    contacts: string,
    /**
     * 封面
     */
    cover: string,
    /**
     * 商家描述
     */
    description: string,
    /**
     * logo
     */
    logo: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 商家名称
     */
    name: string,
    /**
     * 组织id
     */
    orgId: number,
    /**
     * 客服电话
     */
    serviceTel: string,
    /**
     * 特殊资质证明URL
     */
    specialLicenseUrl: string,
    /**
     * 状态（初始化：init，正常：normal，禁用：ban）
     */
    status: string,
    /**
     * 类型 个体经营：individual，企业经营：company
     */
    type: string
  }
  interface QueryMerchantPageCenDTO extends _default.listPagePost {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 商家名称
     */
    name: string,
    /**
     * 商家状态
     */
    status: string
  }
  interface PageDataGetMerchantInfoCenVO {
    /**
     * 数据集合
     */
    rows: _ocenfeignmerchantinfo.GetMerchantInfoCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  /**
   * 新增商家信息 post
   */
  interface _pfeignmerchantinfoadd extends _ocenfeignmerchantinfo.AddMerchantInfoCenDTO {
  }
  /**
   * 新增商家信息 res
   */
  interface _rfeignmerchantinfoadd {
  }
  /**
   * 编辑商家信息 post
   */
  interface _pfeignmerchantinfoedit extends _ocenfeignmerchantinfo.EditMerchantInfoCenDTO {
  }
  /**
   * 编辑商家信息 res
   */
  interface _rfeignmerchantinfoedit {
  }
  /**
   * 查询商家信息 post
   */
  interface _pfeignmerchantinfoget extends _ocenfeignmerchantinfo.GetMerchantCenDTO {
  }
  /**
   * 查询商家信息 res
   */
  interface _rfeignmerchantinfoget extends _ocenfeignmerchantinfo.GetMerchantInfoCenVO {
  }
  /**
   * 商家列表 post
   */
  interface _pfeignmerchantinfoquery extends _ocenfeignmerchantinfo.QueryMerchantPageCenDTO {
  }
  /**
   * 商家列表 res
   */
  interface _rfeignmerchantinfoquery extends _ocenfeignmerchantinfo.PageDataGetMerchantInfoCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 新增商家信息
     */
    _ocenmerchantInfoadd({ data, that }: { data: _ocenfeignmerchantinfo._pfeignmerchantinfoadd, that?: any }): _ocenfeignmerchantinfo._rfeignmerchantinfoadd,
    /**
     * 编辑商家信息
     */
    _ocenmerchantInfoedit({ data, that }: { data: _ocenfeignmerchantinfo._pfeignmerchantinfoedit, that?: any }): _ocenfeignmerchantinfo._rfeignmerchantinfoedit,
    /**
     * 查询商家信息
     */
    _ocenmerchantInfoget({ data, that }: { data: _ocenfeignmerchantinfo._pfeignmerchantinfoget, that?: any }): _ocenfeignmerchantinfo._rfeignmerchantinfoget,
    /**
     * 商家列表
     */
    _ocenmerchantInfoquery({ data, that }: { data: _ocenfeignmerchantinfo._pfeignmerchantinfoquery, that?: any }): _ocenfeignmerchantinfo._rfeignmerchantinfoquery
  }
}
