export default {
  /**
   * post /merchantCenter/merchantstatistics/activeTrend 活跃趋势
   */
  _ocenmerchantstatisticsactiveTrend: { method: 'post', url: '/merchantCenter/merchantstatistics/activeTrend' },
  /**
   * post /merchantCenter/merchantstatistics/integralTrend 积分趋势
   */
  _ocenmerchantstatisticsintegralTrend: { method: 'post', url: '/merchantCenter/merchantstatistics/integralTrend' },
  /**
   * post /merchantCenter/merchantstatistics/registerTrend 注册趋势
   */
  _ocenmerchantstatisticsregisterTrend: { method: 'post', url: '/merchantCenter/merchantstatistics/registerTrend' },
  /**
   * post /merchantCenter/merchantstatistics/scanTrend 扫码趋势
   */
  _ocenmerchantstatisticsscanTrend: { method: 'post', url: '/merchantCenter/merchantstatistics/scanTrend' },
  /**
   * post /merchantCenter/merchantstatistics/todayActiveNumber 当天活跃
   */
  _ocenmerchantstatisticstodayActiveNumber: { method: 'post', url: '/merchantCenter/merchantstatistics/todayActiveNumber' },
  /**
   * post /merchantCenter/merchantstatistics/todayIntegralValue 当天积分
   */
  _ocenmerchantstatisticstodayIntegralValue: { method: 'post', url: '/merchantCenter/merchantstatistics/todayIntegralValue' },
  /**
   * post /merchantCenter/merchantstatistics/todayRegisterNumber 当天注册
   */
  _ocenmerchantstatisticstodayRegisterNumber: { method: 'post', url: '/merchantCenter/merchantstatistics/todayRegisterNumber' },
  /**
   * post /merchantCenter/merchantstatistics/todayScanNumber 当天扫码
   */
  _ocenmerchantstatisticstodayScanNumber: { method: 'post', url: '/merchantCenter/merchantstatistics/todayScanNumber' },
  /**
   * post /merchantCenter/merchantstatistics/totalIntegralValue 总积分
   */
  _ocenmerchantstatisticstotalIntegralValue: { method: 'post', url: '/merchantCenter/merchantstatistics/totalIntegralValue' },
  /**
   * post /merchantCenter/merchantstatistics/totalRegisterNumber 总注册
   */
  _ocenmerchantstatisticstotalRegisterNumber: { method: 'post', url: '/merchantCenter/merchantstatistics/totalRegisterNumber' },
  /**
   * post /merchantCenter/merchantstatistics/totalScanNumber 总扫码
   */
  _ocenmerchantstatisticstotalScanNumber: { method: 'post', url: '/merchantCenter/merchantstatistics/totalScanNumber' }
}
