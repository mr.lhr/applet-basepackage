/**
 * 商家统计
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignmerchantstatistics {
  interface MerchantStatisticsCenDTO extends _default.post {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface BarVO {
    /**
     * 
     */
    datas: undefined[],
    /**
     * 
     */
    xaxis: string[]
  }
  /**
   * 活跃趋势 post
   */
  interface _pfeignmerchantstatisticsactiveTrend extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 活跃趋势 res
   */
  interface _rfeignmerchantstatisticsactiveTrend extends _ocenfeignmerchantstatistics.BarVO {
  }
  /**
   * 积分趋势 post
   */
  interface _pfeignmerchantstatisticsintegralTrend extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 积分趋势 res
   */
  interface _rfeignmerchantstatisticsintegralTrend extends _ocenfeignmerchantstatistics.BarVO {
  }
  /**
   * 注册趋势 post
   */
  interface _pfeignmerchantstatisticsregisterTrend extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 注册趋势 res
   */
  interface _rfeignmerchantstatisticsregisterTrend extends _ocenfeignmerchantstatistics.BarVO {
  }
  /**
   * 扫码趋势 post
   */
  interface _pfeignmerchantstatisticsscanTrend extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 扫码趋势 res
   */
  interface _rfeignmerchantstatisticsscanTrend extends _ocenfeignmerchantstatistics.BarVO {
  }
  /**
   * 当天活跃 post
   */
  interface _pfeignmerchantstatisticstodayActiveNumber extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 当天活跃 res
   */
  interface _rfeignmerchantstatisticstodayActiveNumber {
  }
  /**
   * 当天积分 post
   */
  interface _pfeignmerchantstatisticstodayIntegralValue extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 当天积分 res
   */
  interface _rfeignmerchantstatisticstodayIntegralValue {
  }
  /**
   * 当天注册 post
   */
  interface _pfeignmerchantstatisticstodayRegisterNumber extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 当天注册 res
   */
  interface _rfeignmerchantstatisticstodayRegisterNumber {
  }
  /**
   * 当天扫码 post
   */
  interface _pfeignmerchantstatisticstodayScanNumber extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 当天扫码 res
   */
  interface _rfeignmerchantstatisticstodayScanNumber {
  }
  /**
   * 总积分 post
   */
  interface _pfeignmerchantstatisticstotalIntegralValue extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 总积分 res
   */
  interface _rfeignmerchantstatisticstotalIntegralValue {
  }
  /**
   * 总注册 post
   */
  interface _pfeignmerchantstatisticstotalRegisterNumber extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 总注册 res
   */
  interface _rfeignmerchantstatisticstotalRegisterNumber {
  }
  /**
   * 总扫码 post
   */
  interface _pfeignmerchantstatisticstotalScanNumber extends _ocenfeignmerchantstatistics.MerchantStatisticsCenDTO {
  }
  /**
   * 总扫码 res
   */
  interface _rfeignmerchantstatisticstotalScanNumber {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 活跃趋势
     */
    _ocenmerchantstatisticsactiveTrend({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticsactiveTrend, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticsactiveTrend,
    /**
     * 积分趋势
     */
    _ocenmerchantstatisticsintegralTrend({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticsintegralTrend, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticsintegralTrend,
    /**
     * 注册趋势
     */
    _ocenmerchantstatisticsregisterTrend({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticsregisterTrend, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticsregisterTrend,
    /**
     * 扫码趋势
     */
    _ocenmerchantstatisticsscanTrend({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticsscanTrend, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticsscanTrend,
    /**
     * 当天活跃
     */
    _ocenmerchantstatisticstodayActiveNumber({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticstodayActiveNumber, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticstodayActiveNumber,
    /**
     * 当天积分
     */
    _ocenmerchantstatisticstodayIntegralValue({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticstodayIntegralValue, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticstodayIntegralValue,
    /**
     * 当天注册
     */
    _ocenmerchantstatisticstodayRegisterNumber({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticstodayRegisterNumber, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticstodayRegisterNumber,
    /**
     * 当天扫码
     */
    _ocenmerchantstatisticstodayScanNumber({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticstodayScanNumber, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticstodayScanNumber,
    /**
     * 总积分
     */
    _ocenmerchantstatisticstotalIntegralValue({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticstotalIntegralValue, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticstotalIntegralValue,
    /**
     * 总注册
     */
    _ocenmerchantstatisticstotalRegisterNumber({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticstotalRegisterNumber, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticstotalRegisterNumber,
    /**
     * 总扫码
     */
    _ocenmerchantstatisticstotalScanNumber({ data, that }: { data: _ocenfeignmerchantstatistics._pfeignmerchantstatisticstotalScanNumber, that?: any }): _ocenfeignmerchantstatistics._rfeignmerchantstatisticstotalScanNumber
  }
}
