export default {
  /**
   * post /merchantCenter/merchantAccount/add 添加账号
   */
  _ocenmerchantAccountadd: { method: 'post', url: '/merchantCenter/merchantAccount/add' },
  /**
   * post /merchantCenter/merchantAccount/detail 通过id获取账号详情
   */
  _ocenmerchantAccountdetail: { method: 'post', url: '/merchantCenter/merchantAccount/detail' },
  /**
   * post /merchantCenter/merchantAccount/edit 修改账号信息
   */
  _ocenmerchantAccountedit: { method: 'post', url: '/merchantCenter/merchantAccount/edit' },
  /**
   * post /merchantCenter/merchantAccount/getByAccount 通过账号获取账号详情
   */
  _ocenmerchantAccountgetByAccount: { method: 'post', url: '/merchantCenter/merchantAccount/getByAccount' },
  /**
   * post /merchantCenter/merchantAccount/getByAccountAndPassword 通过账号密码获取账号详情
   */
  _ocenmerchantAccountgetByAccountAndPassword: { method: 'post', url: '/merchantCenter/merchantAccount/getByAccountAndPassword' },
  /**
   * post /merchantCenter/merchantAccount/getByPhone 通过手机号获取账号详情
   */
  _ocenmerchantAccountgetByPhone: { method: 'post', url: '/merchantCenter/merchantAccount/getByPhone' },
  /**
   * post /merchantCenter/merchantAccount/page 分页查询账号信息
   */
  _ocenmerchantAccountpage: { method: 'post', url: '/merchantCenter/merchantAccount/page' },
  /**
   * post /merchantCenter/merchantAccount/personnelCount 统计员工数
   */
  _ocenmerchantAccountpersonnelCount: { method: 'post', url: '/merchantCenter/merchantAccount/personnelCount' }
}
