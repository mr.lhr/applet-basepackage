/**
 * 商家账号
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenmerchantaccount {
  interface AddAccountInfoCenDTO extends _default.post {
    /**
     * 账号
     */
    account: string,
    /**
     * 联系方式
     */
    contactNumber: string,
    /**
     * 邮箱
     */
    email: string,
    /**
     * 头像
     */
    head: string,
    /**
     * 工号
     */
    jobNumber: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 用户名称
     */
    name: string,
    /**
     * 密码
     */
    password: string,
    /**
     * 手机号
     */
    phone: string,
    /**
     * 角色id
     */
    roleId: string
  }
  interface ResultBeanstring extends _default.res {
    /**
     * 
     */
    data: string
  }
  interface AccountInfoDetailCenDTO extends _default.post {
    /**
     * 账号ID
     */
    id: string
  }
  interface ResultBeanAccountDetailCenVO extends _default.res {
    /**
     * 
     */
    data: _ocenmerchantaccount.AccountDetailCenVO
  }
  interface AccountDetailCenVO {
    /**
     * 联系方式
     */
    contactNumber: string,
    /**
     * 账号id
     */
    id: string,
    /**
     * 工号
     */
    jobNumber: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 名称
     */
    name: string,
    /**
     * 密码
     */
    password: string,
    /**
     * 手机号
     */
    phone: string,
    /**
     * 角色id
     */
    roleId: string,
    /**
     * 状态
     */
    status: string
  }
  interface EditAccountCenDTO extends _default.post {
    /**
     * 账号ID
     */
    id: string,
    /**
     * 工号
     */
    jobNumber: string,
    /**
     * 名称
     */
    name: string,
    /**
     * 密码
     */
    password: string,
    /**
     * 手机号
     */
    phone: string,
    /**
     * 角色ID
     */
    roleId: string,
    /**
     * 账号状态
     */
    status: string
  }
  interface GetAccountByAccCenDTO extends _default.post {
    /**
     * 账号
     */
    account: string
  }
  interface GetAccountByAccAndPasCenDTO extends _default.post {
    /**
     * 账号
     */
    account: string,
    /**
     * 密码
     */
    password: string
  }
  interface GetAccountByPhoneCenDTO extends _default.post {
    /**
     * 手机号
     */
    phone: string
  }
  interface AccountPageCenDTO extends _default.post {
    /**
     * 页码
     */
    currentPage: number,
    /**
     * 商家ID
     */
    merchantId: string,
    /**
     * 员工姓名
     */
    name: string,
    /**
     * 角色ID
     */
    roleId: string,
    /**
     * 页面大小
     */
    showCount: number,
    /**
     * 状态
     */
    status: string
  }
  interface ResultBeanPageDataAccountPageCenVO extends _default.res {
    /**
     * 
     */
    data: _ocenmerchantaccount.PageDataAccountPageCenVO
  }
  interface PageDataAccountPageCenVO {
    /**
     * 数据集合
     */
    rows: _ocenmerchantaccount.AccountPageCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  interface AccountPageCenVO {
    /**
     * 联系方式
     */
    contactNumber: string,
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * 账号ID
     */
    id: string,
    /**
     * 员工姓名
     */
    name: string,
    /**
     * 手机号
     */
    phone: string,
    /**
     * 状态
     */
    status: string
  }
  interface PersonnelCountCenDTO extends _default.post {
    /**
     * 商家ID
     */
    merchantId: string
  }
  interface ResultBeanPersonnelCountCenVO extends _default.res {
    /**
     * 
     */
    data: _ocenmerchantaccount.PersonnelCountCenVO
  }
  interface PersonnelCountCenVO {
    /**
     * 停用数
     */
    banCount: number,
    /**
     * 员工数
     */
    staffCount: number
  }
  /**
   * 添加账号 post
   */
  interface _padd extends _ocenmerchantaccount.AddAccountInfoCenDTO {
  }
  /**
   * 添加账号 res
   */
  interface _radd extends _ocenmerchantaccount.ResultBeanstring {
  }
  /**
   * 通过id获取账号详情 post
   */
  interface _pdetail extends _ocenmerchantaccount.AccountInfoDetailCenDTO {
  }
  /**
   * 通过id获取账号详情 res
   */
  interface _rdetail extends _ocenmerchantaccount.ResultBeanAccountDetailCenVO {
  }
  /**
   * 修改账号信息 post
   */
  interface _pedit extends _ocenmerchantaccount.EditAccountCenDTO {
  }
  /**
   * 修改账号信息 res
   */
  interface _redit extends _ocenmerchantaccount.ResultBeanstring {
  }
  /**
   * 通过账号获取账号详情 post
   */
  interface _pgetByAccount extends _ocenmerchantaccount.GetAccountByAccCenDTO {
  }
  /**
   * 通过账号获取账号详情 res
   */
  interface _rgetByAccount extends _ocenmerchantaccount.ResultBeanAccountDetailCenVO {
  }
  /**
   * 通过账号密码获取账号详情 post
   */
  interface _pgetByAccountAndPassword extends _ocenmerchantaccount.GetAccountByAccAndPasCenDTO {
  }
  /**
   * 通过账号密码获取账号详情 res
   */
  interface _rgetByAccountAndPassword extends _ocenmerchantaccount.ResultBeanAccountDetailCenVO {
  }
  /**
   * 通过手机号获取账号详情 post
   */
  interface _pgetByPhone extends _ocenmerchantaccount.GetAccountByPhoneCenDTO {
  }
  /**
   * 通过手机号获取账号详情 res
   */
  interface _rgetByPhone extends _ocenmerchantaccount.ResultBeanAccountDetailCenVO {
  }
  /**
   * 分页查询账号信息 post
   */
  interface _ppage extends _ocenmerchantaccount.AccountPageCenDTO {
  }
  /**
   * 分页查询账号信息 res
   */
  interface _rpage extends _ocenmerchantaccount.ResultBeanPageDataAccountPageCenVO {
  }
  /**
   * 统计员工数 post
   */
  interface _ppersonnelCount extends _ocenmerchantaccount.PersonnelCountCenDTO {
  }
  /**
   * 统计员工数 res
   */
  interface _rpersonnelCount extends _ocenmerchantaccount.ResultBeanPersonnelCountCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 添加账号
     */
    _ocenmerchantAccountadd({ data, that }: { data: _ocenmerchantaccount._padd, that?: any }): _ocenmerchantaccount._radd,
    /**
     * 通过id获取账号详情
     */
    _ocenmerchantAccountdetail({ data, that }: { data: _ocenmerchantaccount._pdetail, that?: any }): _ocenmerchantaccount._rdetail,
    /**
     * 修改账号信息
     */
    _ocenmerchantAccountedit({ data, that }: { data: _ocenmerchantaccount._pedit, that?: any }): _ocenmerchantaccount._redit,
    /**
     * 通过账号获取账号详情
     */
    _ocenmerchantAccountgetByAccount({ data, that }: { data: _ocenmerchantaccount._pgetByAccount, that?: any }): _ocenmerchantaccount._rgetByAccount,
    /**
     * 通过账号密码获取账号详情
     */
    _ocenmerchantAccountgetByAccountAndPassword({ data, that }: { data: _ocenmerchantaccount._pgetByAccountAndPassword, that?: any }): _ocenmerchantaccount._rgetByAccountAndPassword,
    /**
     * 通过手机号获取账号详情
     */
    _ocenmerchantAccountgetByPhone({ data, that }: { data: _ocenmerchantaccount._pgetByPhone, that?: any }): _ocenmerchantaccount._rgetByPhone,
    /**
     * 分页查询账号信息
     */
    _ocenmerchantAccountpage({ data, that }: { data: _ocenmerchantaccount._ppage, that?: any }): _ocenmerchantaccount._rpage,
    /**
     * 统计员工数
     */
    _ocenmerchantAccountpersonnelCount({ data, that }: { data: _ocenmerchantaccount._ppersonnelCount, that?: any }): _ocenmerchantaccount._rpersonnelCount
  }
}
