/**
 * 首页广告
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignhomeadvert {
  interface AddHomeAdvertCenDTO extends _default.post {
    /**
     * 图片
     */
    image: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 跳转路径
     */
    skipUrl: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface EditHomeAdvertCenDTO extends _default.post {
    /**
     * id
     */
    id: string,
    /**
     * 图片
     */
    image: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 跳转路径
     */
    skipUrl: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface HomeAdvertCenVO {
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * id
     */
    id: string,
    /**
     * 图片
     */
    image: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 跳转路径
     */
    skipUrl: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface QueryHomeAdvertCenDTO extends _default.listPagePost {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface PageDataHomeAdvertCenVO {
    /**
     * 数据集合
     */
    rows: _ocenfeignhomeadvert.HomeAdvertCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  /**
   * 新增首页广告 post
   */
  interface _pfeignhomeadvertadd extends _ocenfeignhomeadvert.AddHomeAdvertCenDTO {
  }
  /**
   * 新增首页广告 res
   */
  interface _rfeignhomeadvertadd {
  }
  /**
   * 编辑首页广告 post
   */
  interface _pfeignhomeadvertedit extends _ocenfeignhomeadvert.EditHomeAdvertCenDTO {
  }
  /**
   * 编辑首页广告 res
   */
  interface _rfeignhomeadvertedit {
  }
  /**
   * 首页广告详情 get
   */
  interface _pfeignhomeadvertget {
  }
  /**
   * 首页广告详情 res
   */
  interface _rfeignhomeadvertget extends _ocenfeignhomeadvert.HomeAdvertCenVO {
  }
  /**
   * 查询首页广告 post
   */
  interface _pfeignhomeadvertquery extends _ocenfeignhomeadvert.QueryHomeAdvertCenDTO {
  }
  /**
   * 查询首页广告 res
   */
  interface _rfeignhomeadvertquery extends _ocenfeignhomeadvert.PageDataHomeAdvertCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 新增首页广告
     */
    _ocenhomeadvertadd({ data, that }: { data: _ocenfeignhomeadvert._pfeignhomeadvertadd, that?: any }): _ocenfeignhomeadvert._rfeignhomeadvertadd,
    /**
     * 编辑首页广告
     */
    _ocenhomeadvertedit({ data, that }: { data: _ocenfeignhomeadvert._pfeignhomeadvertedit, that?: any }): _ocenfeignhomeadvert._rfeignhomeadvertedit,
    /**
     * 首页广告详情
     */
    _ocenhomeadvertget({ data, that }: { data: _ocenfeignhomeadvert._pfeignhomeadvertget, that?: any }): _ocenfeignhomeadvert._rfeignhomeadvertget,
    /**
     * 查询首页广告
     */
    _ocenhomeadvertquery({ data, that }: { data: _ocenfeignhomeadvert._pfeignhomeadvertquery, that?: any }): _ocenfeignhomeadvert._rfeignhomeadvertquery
  }
}
