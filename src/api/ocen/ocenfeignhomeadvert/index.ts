export default {
  /**
   * post /feign/merchant/homeadvert/add 新增首页广告
   */
  _ocenhomeadvertadd: { method: 'post', url: '/feign/merchant/homeadvert/add' },
  /**
   * post /feign/merchant/homeadvert/edit 编辑首页广告
   */
  _ocenhomeadvertedit: { method: 'post', url: '/feign/merchant/homeadvert/edit' },
  /**
   * get /feign/merchant/homeadvert/get 首页广告详情
   */
  _ocenhomeadvertget: { method: 'get', url: '/feign/merchant/homeadvert/get' },
  /**
   * post /feign/merchant/homeadvert/query 查询首页广告
   */
  _ocenhomeadvertquery: { method: 'post', url: '/feign/merchant/homeadvert/query' }
}
