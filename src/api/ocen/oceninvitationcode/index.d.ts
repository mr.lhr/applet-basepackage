/**
 * 邀请码
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _oceninvitationcode {
  interface CheckCodeCenDTO extends _default.post {
    /**
     * 邀请码
     */
    invitationCode: string
  }
  interface ResultBeanstring extends _default.res {
    /**
     * 
     */
    data: string
  }
  /**
   * 校验邀请码 post
   */
  interface _pcheckInvitationCode extends _oceninvitationcode.CheckCodeCenDTO {
  }
  /**
   * 校验邀请码 res
   */
  interface _rcheckInvitationCode extends _oceninvitationcode.ResultBeanstring {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 校验邀请码
     */
    _ocenInvitationCodecheckInvitationCode({ data, that }: { data: _oceninvitationcode._pcheckInvitationCode, that?: any }): _oceninvitationcode._rcheckInvitationCode
  }
}
