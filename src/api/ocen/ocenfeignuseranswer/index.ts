export default {
  /**
   * get /feign/merchant/useranswer/queryAnswer 查询用户答案
   */
  _ocenuseranswerqueryAnswer: { method: 'get', url: '/feign/merchant/useranswer/queryAnswer' },
  /**
   * post /feign/merchant/useranswer/queryQuestionnaire 查询用户答卷
   */
  _ocenuseranswerqueryQuestionnaire: { method: 'post', url: '/feign/merchant/useranswer/queryQuestionnaire' }
}
