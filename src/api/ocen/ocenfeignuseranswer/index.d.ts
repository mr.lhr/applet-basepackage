/**
 * 用户答卷
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeignuseranswer {
  interface QueryUserAnswerCenDTO extends _default.listPagePost {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 问卷id
     */
    questionnaireId: string,
    /**
     * 店铺id
     */
    storeId: string,
    /**
     * 用户id
     */
    userId: string
  }
  interface QueryUserQuestionnaireCenDTO extends _default.listPagePost {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 问卷id
     */
    questionnaireId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface PageDataUserQuestionnaireCenVO {
    /**
     * 数据集合
     */
    rows: _ocenfeignuseranswer.UserQuestionnaireCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  interface UserQuestionnaireCenVO {
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * id
     */
    id: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 问卷id
     */
    questionnaireId: string,
    /**
     * 问卷名称
     */
    questionnaireName: string,
    /**
     * 店铺id
     */
    storeId: string,
    /**
     * 会员id
     */
    userId: string
  }
  /**
   * 查询用户答案 get
   */
  interface _pfeignuseranswerqueryAnswer extends _ocenfeignuseranswer.QueryUserAnswerCenDTO {
  }
  /**
   * 查询用户答案 res
   */
  interface _rfeignuseranswerqueryAnswer {
  }
  /**
   * 查询用户答卷 post
   */
  interface _pfeignuseranswerqueryQuestionnaire extends _ocenfeignuseranswer.QueryUserQuestionnaireCenDTO {
  }
  /**
   * 查询用户答卷 res
   */
  interface _rfeignuseranswerqueryQuestionnaire extends _ocenfeignuseranswer.PageDataUserQuestionnaireCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 查询用户答案
     */
    _ocenuseranswerqueryAnswer({ data, that }: { data: _ocenfeignuseranswer._pfeignuseranswerqueryAnswer, that?: any }): _ocenfeignuseranswer._rfeignuseranswerqueryAnswer,
    /**
     * 查询用户答卷
     */
    _ocenuseranswerqueryQuestionnaire({ data, that }: { data: _ocenfeignuseranswer._pfeignuseranswerqueryQuestionnaire, that?: any }): _ocenfeignuseranswer._rfeignuseranswerqueryQuestionnaire
  }
}
