export default {
  /**
   * post /merchantCenter/merchantRole/addRole 添加角色
   */
  _ocenmerchantRoleaddRole: { method: 'post', url: '/merchantCenter/merchantRole/addRole' },
  /**
   * post /merchantCenter/merchantRole/allRoleList 商家所有角色列表
   */
  _ocenmerchantRoleallRoleList: { method: 'post', url: '/merchantCenter/merchantRole/allRoleList' },
  /**
   * post /merchantCenter/merchantRole/delRole 删除角色
   */
  _ocenmerchantRoledelRole: { method: 'post', url: '/merchantCenter/merchantRole/delRole' },
  /**
   * post /merchantCenter/merchantRole/editRole 编辑角色
   */
  _ocenmerchantRoleeditRole: { method: 'post', url: '/merchantCenter/merchantRole/editRole' },
  /**
   * post /merchantCenter/merchantRole/queryRoleInfo 查询角色信息
   */
  _ocenmerchantRolequeryRoleInfo: { method: 'post', url: '/merchantCenter/merchantRole/queryRoleInfo' },
  /**
   * post /merchant_center/merchantWeChatInfo/getInfo 获取微信配置
   */
  _ocenmerchantWeChatInfogetInfo: { method: 'post', url: '/merchant_center/merchantWeChatInfo/getInfo' }
}
