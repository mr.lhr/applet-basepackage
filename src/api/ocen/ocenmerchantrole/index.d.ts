/**
 * 商家角色
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenmerchantrole {
  interface AddRoleCenDTO extends _default.post {
    /**
     * 代码
     */
    code: string,
    /**
     * 角色名称
     */
    name: string
  }
  interface ResultBeanstring extends _default.res {
    /**
     * 
     */
    data: string
  }
  interface AllRoleListCenDTO extends _default.post {
  }
  interface ResultBeanListAllRoleListCenVO extends _default.res {
    /**
     * 
     */
    data: _ocenmerchantrole.AllRoleListCenVO[]
  }
  interface AllRoleListCenVO {
    /**
     * 角色ID
     */
    id: string,
    /**
     * 角色名称
     */
    name: string,
    /**
     * 权限指
     */
    rights: string
  }
  interface DelRoleCenDTO extends _default.post {
    /**
     * 角色ID
     */
    id: string
  }
  interface EditRoleCenDTO extends _default.post {
    /**
     * 角色ID
     */
    id: string,
    /**
     * 角色名称
     */
    name: string,
    /**
     * 权限指
     */
    rights: string
  }
  interface QueryRoleInfoCenDTO extends _default.listPagePost {
    /**
     * 角色ID
     */
    id: string
  }
  interface ResultBeanQueryRoleInfoCenVO extends _default.res {
    /**
     * 
     */
    data: _ocenmerchantrole.QueryRoleInfoCenVO
  }
  interface QueryRoleInfoCenVO {
    /**
     * 角色ID
     */
    id: string,
    /**
     * 角色ID
     */
    name: string,
    /**
     * 角色ID
     */
    rights: string
  }
  interface GetMerchantWeChatInfoDTO extends _default.post {
    /**
     * 应用类型
     */
    appType: string,
    /**
     * 店铺id
     */
    storeId: number
  }
  interface ResultBeanGetMerchantWeChatInfoVO extends _default.res {
    /**
     * 
     */
    data: _ocenmerchantrole.GetMerchantWeChatInfoVO
  }
  interface GetMerchantWeChatInfoVO {
    /**
     * APPID
     */
    appId: string,
    /**
     * AppSecret
     */
    appSecret: string,
    /**
     * 用类型（公众号：official，小程序：applet，商户号：merchant）
     */
    appType: string,
    /**
     * 商家标识
     */
    merchantId: number,
    /**
     * 请求系统编码
     */
    reqCode: string,
    /**
     * 请求时间
     */
    reqTime: string,
    /**
     * 店铺id
     */
    storeId: number
  }
  /**
   * 添加角色 post
   */
  interface _paddRole extends _ocenmerchantrole.AddRoleCenDTO {
  }
  /**
   * 添加角色 res
   */
  interface _raddRole extends _ocenmerchantrole.ResultBeanstring {
  }
  /**
   * 商家所有角色列表 post
   */
  interface _pallRoleList extends _ocenmerchantrole.AllRoleListCenDTO {
  }
  /**
   * 商家所有角色列表 res
   */
  interface _rallRoleList extends _ocenmerchantrole.ResultBeanListAllRoleListCenVO {
  }
  /**
   * 删除角色 post
   */
  interface _pdelRole extends _ocenmerchantrole.DelRoleCenDTO {
  }
  /**
   * 删除角色 res
   */
  interface _rdelRole extends _ocenmerchantrole.ResultBeanstring {
  }
  /**
   * 编辑角色 post
   */
  interface _peditRole extends _ocenmerchantrole.EditRoleCenDTO {
  }
  /**
   * 编辑角色 res
   */
  interface _reditRole extends _ocenmerchantrole.ResultBeanstring {
  }
  /**
   * 查询角色信息 post
   */
  interface _pqueryRoleInfo extends _ocenmerchantrole.QueryRoleInfoCenDTO {
  }
  /**
   * 查询角色信息 res
   */
  interface _rqueryRoleInfo extends _ocenmerchantrole.ResultBeanQueryRoleInfoCenVO {
  }
  /**
   * 获取微信配置 post
   */
  interface _pmerchantrolegetInfo extends _ocenmerchantrole.GetMerchantWeChatInfoDTO {
  }
  /**
   * 获取微信配置 res
   */
  interface _rmerchantrolegetInfo extends _ocenmerchantrole.ResultBeanGetMerchantWeChatInfoVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 添加角色
     */
    _ocenmerchantRoleaddRole({ data, that }: { data: _ocenmerchantrole._paddRole, that?: any }): _ocenmerchantrole._raddRole,
    /**
     * 商家所有角色列表
     */
    _ocenmerchantRoleallRoleList({ data, that }: { data: _ocenmerchantrole._pallRoleList, that?: any }): _ocenmerchantrole._rallRoleList,
    /**
     * 删除角色
     */
    _ocenmerchantRoledelRole({ data, that }: { data: _ocenmerchantrole._pdelRole, that?: any }): _ocenmerchantrole._rdelRole,
    /**
     * 编辑角色
     */
    _ocenmerchantRoleeditRole({ data, that }: { data: _ocenmerchantrole._peditRole, that?: any }): _ocenmerchantrole._reditRole,
    /**
     * 查询角色信息
     */
    _ocenmerchantRolequeryRoleInfo({ data, that }: { data: _ocenmerchantrole._pqueryRoleInfo, that?: any }): _ocenmerchantrole._rqueryRoleInfo,
    /**
     * 获取微信配置
     */
    _ocenmerchantWeChatInfogetInfo({ data, that }: { data: _ocenmerchantrole._pmerchantrolegetInfo, that?: any }): _ocenmerchantrole._rmerchantrolegetInfo
  }
}
