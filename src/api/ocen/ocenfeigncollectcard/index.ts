export default {
  /**
   * post /feign/merchant/collectcard/add 新增收集卡
   */
  _ocencollectcardadd: { method: 'post', url: '/feign/merchant/collectcard/add' },
  /**
   * post /feign/merchant/collectcard/edit 编辑收集卡
   */
  _ocencollectcardedit: { method: 'post', url: '/feign/merchant/collectcard/edit' },
  /**
   * get /feign/merchant/collectcard/get 收集卡详情
   */
  _ocencollectcardget: { method: 'get', url: '/feign/merchant/collectcard/get' },
  /**
   * post /feign/merchant/collectcard/query 查询收集卡
   */
  _ocencollectcardquery: { method: 'post', url: '/feign/merchant/collectcard/query' }
}
