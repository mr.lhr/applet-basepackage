/**
 * 收集卡
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeigncollectcard {
  interface AddCollectCardCenDTO extends _default.post {
    /**
     * 描述
     */
    description: string,
    /**
     * 图片
     */
    image: string,
    /**
     * 收集卡等级
     */
    level: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 收集卡名称
     */
    name: string,
    /**
     * 系列id
     */
    seriaId: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface EditCollectCardCenDTO extends _default.post {
    /**
     * 描述
     */
    description: string,
    /**
     * id
     */
    id: string,
    /**
     * 图片
     */
    image: string,
    /**
     * 收集卡等级
     */
    level: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 收集卡名称
     */
    name: string,
    /**
     * 系列id
     */
    seriaId: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface GetCollectCardCenDTO extends _default.post {
    /**
     * id
     */
    id: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface CollectCardCenVO {
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * 描述
     */
    description: string,
    /**
     * id
     */
    id: string,
    /**
     * 图片
     */
    image: string,
    /**
     * 收集卡等级
     */
    level: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 收集卡名称
     */
    name: string,
    /**
     * 系列id
     */
    seriaId: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface QueryCollectCardCenDTO extends _default.listPagePost {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 收集卡名称
     */
    name: string,
    /**
     * 系列id
     */
    seriesId: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface PageDataCollectCardCenVO {
    /**
     * 数据集合
     */
    rows: _ocenfeigncollectcard.CollectCardCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  /**
   * 新增收集卡 post
   */
  interface _pfeigncollectcardadd extends _ocenfeigncollectcard.AddCollectCardCenDTO {
  }
  /**
   * 新增收集卡 res
   */
  interface _rfeigncollectcardadd {
  }
  /**
   * 编辑收集卡 post
   */
  interface _pfeigncollectcardedit extends _ocenfeigncollectcard.EditCollectCardCenDTO {
  }
  /**
   * 编辑收集卡 res
   */
  interface _rfeigncollectcardedit {
  }
  /**
   * 收集卡详情 get
   */
  interface _pfeigncollectcardget extends _ocenfeigncollectcard.GetCollectCardCenDTO {
  }
  /**
   * 收集卡详情 res
   */
  interface _rfeigncollectcardget extends _ocenfeigncollectcard.CollectCardCenVO {
  }
  /**
   * 查询收集卡 post
   */
  interface _pfeigncollectcardquery extends _ocenfeigncollectcard.QueryCollectCardCenDTO {
  }
  /**
   * 查询收集卡 res
   */
  interface _rfeigncollectcardquery extends _ocenfeigncollectcard.PageDataCollectCardCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 新增收集卡
     */
    _ocencollectcardadd({ data, that }: { data: _ocenfeigncollectcard._pfeigncollectcardadd, that?: any }): _ocenfeigncollectcard._rfeigncollectcardadd,
    /**
     * 编辑收集卡
     */
    _ocencollectcardedit({ data, that }: { data: _ocenfeigncollectcard._pfeigncollectcardedit, that?: any }): _ocenfeigncollectcard._rfeigncollectcardedit,
    /**
     * 收集卡详情
     */
    _ocencollectcardget({ data, that }: { data: _ocenfeigncollectcard._pfeigncollectcardget, that?: any }): _ocenfeigncollectcard._rfeigncollectcardget,
    /**
     * 查询收集卡
     */
    _ocencollectcardquery({ data, that }: { data: _ocenfeigncollectcard._pfeigncollectcardquery, that?: any }): _ocenfeigncollectcard._rfeigncollectcardquery
  }
}
