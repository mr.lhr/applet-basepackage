/**
 * 收集卡系列
 * create: lihuarong<517849815@qq.com>
 * time: 2021-04-15 16:19:27
 */
declare namespace _ocenfeigncollectcardseries {
  interface AddCollectCardSeriesCenDTO extends _default.post {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 系列名
     */
    name: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface EditCollectCardSeriesCenDTO extends _default.post {
    /**
     * id
     */
    id: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 系列名
     */
    name: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface CollectCardSeriesCenVO {
    /**
     * 创建时间
     */
    createTime: string,
    /**
     * id
     */
    id: string,
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 系列名
     */
    name: string,
    /**
     * 显示次序
     */
    showIndex: number,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface QueryCollectCardSeriesCenDTO extends _default.listPagePost {
    /**
     * 商家id
     */
    merchantId: string,
    /**
     * 系列名
     */
    name: string,
    /**
     * 店铺id
     */
    storeId: string
  }
  interface PageDataCollectCardSeriesCenVO {
    /**
     * 数据集合
     */
    rows: _ocenfeigncollectcardseries.CollectCardSeriesCenVO[],
    /**
     * 总行数
     */
    total: number
  }
  /**
   * 新增系列 post
   */
  interface _pfeigncollectcardseriesadd extends _ocenfeigncollectcardseries.AddCollectCardSeriesCenDTO {
  }
  /**
   * 新增系列 res
   */
  interface _rfeigncollectcardseriesadd {
  }
  /**
   * 编辑系列 post
   */
  interface _pfeigncollectcardseriesedit extends _ocenfeigncollectcardseries.EditCollectCardSeriesCenDTO {
  }
  /**
   * 编辑系列 res
   */
  interface _rfeigncollectcardseriesedit {
  }
  /**
   * 系列详情 get
   */
  interface _pfeigncollectcardseriesget {
  }
  /**
   * 系列详情 res
   */
  interface _rfeigncollectcardseriesget extends _ocenfeigncollectcardseries.CollectCardSeriesCenVO {
  }
  /**
   * 收集卡系列 get
   */
  interface _pfeigncollectcardserieslist {
  }
  /**
   * 收集卡系列 res
   */
  interface _rfeigncollectcardserieslist {
  }
  /**
   * 查询系列 post
   */
  interface _pfeigncollectcardseriesquery extends _ocenfeigncollectcardseries.QueryCollectCardSeriesCenDTO {
  }
  /**
   * 查询系列 res
   */
  interface _rfeigncollectcardseriesquery extends _ocenfeigncollectcardseries.PageDataCollectCardSeriesCenVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 新增系列
     */
    _ocencollectcardseriesadd({ data, that }: { data: _ocenfeigncollectcardseries._pfeigncollectcardseriesadd, that?: any }): _ocenfeigncollectcardseries._rfeigncollectcardseriesadd,
    /**
     * 编辑系列
     */
    _ocencollectcardseriesedit({ data, that }: { data: _ocenfeigncollectcardseries._pfeigncollectcardseriesedit, that?: any }): _ocenfeigncollectcardseries._rfeigncollectcardseriesedit,
    /**
     * 系列详情
     */
    _ocencollectcardseriesget({ data, that }: { data: _ocenfeigncollectcardseries._pfeigncollectcardseriesget, that?: any }): _ocenfeigncollectcardseries._rfeigncollectcardseriesget,
    /**
     * 收集卡系列
     */
    _ocencollectcardserieslist({ data, that }: { data: _ocenfeigncollectcardseries._pfeigncollectcardserieslist, that?: any }): _ocenfeigncollectcardseries._rfeigncollectcardserieslist,
    /**
     * 查询系列
     */
    _ocencollectcardseriesquery({ data, that }: { data: _ocenfeigncollectcardseries._pfeigncollectcardseriesquery, that?: any }): _ocenfeigncollectcardseries._rfeigncollectcardseriesquery
  }
}
