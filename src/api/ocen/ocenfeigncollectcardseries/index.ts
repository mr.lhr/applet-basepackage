export default {
  /**
   * post /feign/merchant/collectcardseries/add 新增系列
   */
  _ocencollectcardseriesadd: { method: 'post', url: '/feign/merchant/collectcardseries/add' },
  /**
   * post /feign/merchant/collectcardseries/edit 编辑系列
   */
  _ocencollectcardseriesedit: { method: 'post', url: '/feign/merchant/collectcardseries/edit' },
  /**
   * get /feign/merchant/collectcardseries/get 系列详情
   */
  _ocencollectcardseriesget: { method: 'get', url: '/feign/merchant/collectcardseries/get' },
  /**
   * get /feign/merchant/collectcardseries/list 收集卡系列
   */
  _ocencollectcardserieslist: { method: 'get', url: '/feign/merchant/collectcardseries/list' },
  /**
   * post /feign/merchant/collectcardseries/query 查询系列
   */
  _ocencollectcardseriesquery: { method: 'post', url: '/feign/merchant/collectcardseries/query' }
}
