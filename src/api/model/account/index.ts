export default {
  /**
   * post /topApp/visitor/account/wxLogin 微信登录
   */
  _accountwxLogin: { method: 'post', url: '/topApp/visitor/account/wxLogin' },
  /**
   * post /topApp/visitor/account/wxRegister 微信注册
   */
  _accountwxRegister: { method: 'post', url: '/topApp/visitor/account/wxRegister' }
}
