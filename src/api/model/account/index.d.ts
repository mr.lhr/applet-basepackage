/**
 * 账号
 * create: lihuarong<517849815@qq.com>
 * time: 2020-06-22 18:16:45
 */
declare namespace _account {
  interface WxLoginCliDTO {
    /**
     * 应用标识
     */
    appKey: string,
    /**
     * 微信登录 code
     */
    jsCode: string,
    /**
     * 请求系统编码
     */
    reqCode: string,
    /**
     * 请求时间
     */
    reqTime: string
  }
  interface ResultBeanWXLoginCliVO extends _default.res {
    /**
     * 
     */
    data: _account.WXLoginCliVO,
    /**
     * 
     */
    success: boolean
  }
  interface WXLoginCliVO {
    /**
     * Token
     */
    token: string
  }
  interface WXRegisterCliDTO {
    /**
     * 头像
     */
    avatarUrl: string,
    /**
     * 城市
     */
    city: string,
    /**
     * 国家
     */
    country: string,
    /**
     * 微信加密数据
     */
    encryptedData: string,
    /**
     * 性别
     */
    gender: number,
    /**
     * 微信初始向量
     */
    iv: string,
    /**
     * 登录时获取的 code
     */
    jsCode: string,
    /**
     * 昵称
     */
    nickName: string,
    /**
     * 请求系统编码
     */
    reqCode: string,
    /**
     * 请求时间
     */
    reqTime: string
  }
  interface ResultBeanWXRegisterCliVO extends _default.res {
    /**
     * 
     */
    data: _account.WXRegisterCliVO,
    /**
     * 
     */
    success: boolean
  }
  interface WXRegisterCliVO {
    /**
     * Token
     */
    token: string
  }
  /**
   * 微信登录 post
   */
  interface _pwxLogin extends _account.WxLoginCliDTO {
  }
  /**
   * 微信登录 res
   */
  interface _rwxLogin extends _account.ResultBeanWXLoginCliVO {
  }
  /**
   * 微信注册 post
   */
  interface _pwxRegister extends _account.WXRegisterCliDTO {
  }
  /**
   * 微信注册 res
   */
  interface _rwxRegister extends _account.ResultBeanWXRegisterCliVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 微信登录
     */
    _accountwxLogin({ data, taht }: { data: _account._pwxLogin, taht?: any }): _account._rwxLogin,
    /**
     * 微信注册
     */
    _accountwxRegister({ data, taht }: { data: _account._pwxRegister, taht?: any }): _account._rwxRegister
  }
}
