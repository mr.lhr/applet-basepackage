/**
 * 个人信息
 * create: lihuarong<517849815@qq.com>
 * time: 2020-06-22 18:16:44
 */
declare namespace _memberinfo {
  interface EditMemberInfoCliDTO {
    /**
     * 头像
     */
    avatarUrl: string,
    /**
     * 生日
     */
    birthday: string,
    /**
     * 城市
     */
    city: string,
    /**
     * 国家
     */
    country: string,
    /**
     * 性别
     */
    gender: number,
    /**
     * 昵称
     */
    nickName: string,
    /**
     * 请求系统编码
     */
    reqCode: string,
    /**
     * 请求时间
     */
    reqTime: string
  }
  interface ResultBeanstring extends _default.res {
    /**
     * 
     */
    data: string,
    /**
     * 
     */
    success: boolean
  }
  interface Request extends _default.post {
  }
  interface ResultBeanQueryMemberInfoCliVO extends _default.res {
    /**
     * 
     */
    data: _memberinfo.QueryMemberInfoCliVO,
    /**
     * 
     */
    success: boolean
  }
  interface QueryMemberInfoCliVO {
    /**
     * 头像
     */
    avatarUrl: string,
    /**
     * 生日
     */
    birthday: string,
    /**
     * 城市
     */
    city: string,
    /**
     * 国家
     */
    country: string,
    /**
     * 性别
     */
    gender: number,
    /**
     * 昵称
     */
    nickName: string
  }
  /**
   * 修改个人信息 post
   */
  interface _pedit extends _memberinfo.EditMemberInfoCliDTO {
  }
  /**
   * 修改个人信息 res
   */
  interface _redit extends _memberinfo.ResultBeanstring {
  }
  /**
   * 查询个人信息 post
   */
  interface _pquery extends _memberinfo.Request {
  }
  /**
   * 查询个人信息 res
   */
  interface _rquery extends _memberinfo.ResultBeanQueryMemberInfoCliVO {
  }
}
declare module _default {
  interface apifuns {
    /**
     * 修改个人信息
     */
    _memberInfoedit({ data, taht }: { data: _memberinfo._pedit, taht?: any }): _memberinfo._redit,
    /**
     * 查询个人信息
     */
    _memberInfoquery({ data, taht }: { data: _memberinfo._pquery, taht?: any }): _memberinfo._rquery
  }
}
