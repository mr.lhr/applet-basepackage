export default {
  /**
   * post /topApp/memberInfo/edit 修改个人信息
   */
  _memberInfoedit: { method: 'post', url: '/topApp/memberInfo/edit' },
  /**
   * post /topApp/memberInfo/query 查询个人信息
   */
  _memberInfoquery: { method: 'post', url: '/topApp/memberInfo/query' }
}
