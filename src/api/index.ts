import Vue from 'vue'
import request from './request'
import urls from './RESTFULLURL'

const FUNS = {} as any
const urlObj: any = urls

Object.keys(urlObj).forEach((key) => {
  FUNS[key] = (options: any = {}) => {
    const obj: any = urlObj[key]
    options.method = obj.method || 'post'
    const that = options.that
    delete options.that
    if (/{.*}/.test(obj.url) && options.data) {
      let arr = obj.url.split('/')
      arr = arr.map((item: string) => {
        if (!/{.*}/.test(item)) return item
        const temp = item.replace(/{|}/g, '')
        item = JSON.parse(JSON.stringify(options.data[temp]))
        delete options.data[temp]
        return item
      })
      obj.url = arr.join('/')
    }
    return request(obj.url, options, that)
  }
})

// 将API-model挂载到vue的原型上
// views引用的方法：this.$API 接口名（小驼峰）
Object.defineProperty(Vue.prototype, '$API', { value: FUNS })

export default FUNS
