import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import store from './store'
Vue.prototype.$store = store

import './api'

import './libs/utilFun'
import './libs/projectFun'

new App().$mount()
