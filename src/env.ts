
/**
 * 接口地址
 */
let host = 'http://192.168.3.220:8003' as string

/**
 * 二维码格式
 */
let codeType = 'test?c=' as string

/**
 * 仓库 key值
 */
let storeKey = 'applet'

/**
 * 图片上传桶
 */
interface wxuploadM {
  SecretId: string,
  SecretKey: string,
  Bucket: string,
  Region: string
}

let uploadVal = {
  SecretId: "",
  SecretKey: "",
  Bucket: "wjh-t-1300238208" /* 必须 */,
  Region: "ap-guangzhou" /* 存储桶所在地域，必须字段 */
} as wxuploadM

if (process.env.NODE_ENV === 'production') {
  host = ''
  codeType = 'm?c='
  uploadVal.Bucket = 'wjh-1300238208'
}

export {
  host,
  codeType,
  uploadVal,
  storeKey
}