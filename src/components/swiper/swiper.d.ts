/**
 * 滑动组件 模型
 * by: Lihuarong
 * time: 2020年4月23日 10点46分
 */

declare namespace swiper {
	/**
	 *  背景模型
	 */ 
	interface bgMod {
		/**
		 *  是否显示
		 */ 
		show?: boolean,
	}
	
	/**
	 *  图片数据模型
	 */ 
	interface swiperMod {
		/**
		 *  图片链接
		 */ 
		src: string,

		/**
		 *  跳转链接
		 */ 
		link: string,

		/**
		 *  跳转模式 
		 */ 
		linkType?: string, 
	}

	/**
	 * 指示点模型
	 */
	interface dotsM {
		/**
		 * 底部距离 默认 16rpx
		 */
		bottom?: string,
		/**
		 * 高度 默认 4rpx
		 */
		height?: string,
		/**
		 * 宽度 默认 10rpx
		 */
		width?: string,
		/**
		 * 激活颜色 默认 #FFFFFF
		 */
		actionColor?: string,
		/**
		 * 颜色 默认 #B5B5B5
		 */
		color?: string,
		/**
		 * 圆角大小 默认 2rpx
		 */
		radius?: string,
		/**
		 * 间隔 默认 8rpx
		 */
		left?: string,
	}
	
	/**
	 * 配置模型
	 */ 
	 interface configMod {
		/**
		 *  容器的高度
		 */ 
		height?: string,

		/**
		 *  swiper 样式
		 */ 
		 swiperStyle?: object,
		
		/**
		 *  图片的样式
		 */ 
		configimageStyle?: object,

		/**
		 *  背景是否设置
		 */ 
		bg?: bgMod,

		/**
		 *  放大
		 */ 
		draw?: boolean,
		
		/**
		 *  轮播的图片数组
		 */ 
		swiperArr?: Array<swiperMod>,
		
		/**
		 *  是否显示面板指示点 flase
		 */ 
		indicator_dots?: boolean,
		
		/**
		 *  指示点样式
		 */ 
		indicator_color?: string,
		
		/**
		 *  当前指示点样式
		 */ 
		indicator_active_color?: string,
		
		/**
		 *  是否自动切换 默认false
		 */ 
		autoplay?: boolean,
		
		/**
		 *  当前所在滑块的 index
		 */ 
		current?: number,
		
		/**
		 *  自动切换时间间隔 默认5000
		 */ 
		interval?: number,
		
		/**
		 *  是否采用衔接滑动 默认false
		 */ 
		circular?: boolean,
		
		/**
		 *  滑动方向是否为纵向 默认false
		 */ 
		vertical?: boolean,

		/**
		 *  前边距，可用于露出前一项的一小部分，接受 px 和 rpx 值
		 */ 
		previous_margin?: string,

		/**
		 *  后边距，可用于露出后一项的一小部分，接受 px 和 rpx 值
		 */ 
		next_margin?: string,

		/**
		 * 自定义指示点
		 */
		dots?: dotsM
	}
} 
