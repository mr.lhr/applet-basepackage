/**
 * 文章组件模型
 * by: Lihuarong
 * time: 2020年4月30日 10点49分
 */

 declare namespace articleCom {

  // 样式
  interface cssMod {
    // 背景
    bg?: string,

    // 字体颜色
    color?: string,

    // 样式
    style?: Object
  }

  // 文章模型
  interface title extends cssMod {
    // 内容
    text: string,
  }

  // 发布者
  interface publisher extends cssMod {
    // 发布者头像
    head?: string,

    // 名称
    name: string
  }

  interface config {
    // 文章标题
    title: title,

    // 发布者
    publisher?: publisher,

    // 时间
    createTime: string,

    // 类型
    type: string,

    // 内容
    content?: string,

    // 视频地址
    videoUrl?: string

  }

 }