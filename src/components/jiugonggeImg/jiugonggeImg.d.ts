/**
 * 九宫格组件模型
 * by: Lihuarong
 * time: 2020年4月04日 16点19分
 */

declare namespace jiugonggeImg {

  interface config {

    // 是否有字段匹配
    prop?: string,

    // 样式
    style?: any,

    // 图片总的宽度 
    width?: number,

    // 图片边距
    margin?: number,

    // 图片总图层左边
    left?: number,
  }

}