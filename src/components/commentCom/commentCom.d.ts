/**
 * 评论组件模型
 * by: Lihuarong
 * time: 2020年4月30日 13点49分
 */

declare namespace commentCom {

  interface model {

  }

  interface config {

    // 数据
    commentData: Array<any>,

    // 总数
    total: number,

    // 数据key
    prop?: {
      // 评论人头像
      avatarUrl: string,

      // 评论时间
      createTime: string,

      // 评价内容
      details: string,

      // 昵称
      nickName: string,

      // 回复评论数量
      replyNumber: string,
    }

    // 点击回调
    onClick?: any,

    // 点击回复的回掉
    replyOnclick?: any,
    


  }

}