/**
 * 用户仓库
 * by: lihuarong
 * time: 2020年6月23日 13点50分
 */
import { VuexModule, Module as vueMoudle, Action, Mutation, getModule } from 'vuex-module-decorators'
import store from '@/store'
import localStore from '@/store/store'

const key = 'userinfo' as string

export interface IUserinfo {
  userInfo: {
    token: string,
    isLogo: boolean
  }
}

@vueMoudle({ dynamic: true, store, name: key })
class Userinfo extends VuexModule implements IUserinfo {
  public userInfo = localStore.get({ key })

  /**
   * 更新用户数据
   */
  @Mutation
  private async SET_USERINFO(value: object) {
    await localStore.set({ key, value })
    this.userInfo = await localStore.get({ key })
    return true
  }

  /**
   * 删除用户数据
   */
  @Mutation
  private async DELETE_USERINFO(value: object = {}) {
    await localStore.set({ key,  value })
    this.userInfo = await localStore.get({ key })
    return true
  }

  /**
   * 更新用户数据
   */
  @Action
  public async update_userinfo(value: object) {
    await this.SET_USERINFO(value)
    return true
  }

  /**
   * 删除用户数据
   */
  @Action
  public async delete_userinfo() {
    await this.DELETE_USERINFO()
    return true
  }
}

export const UserinfoModule = getModule(Userinfo)
