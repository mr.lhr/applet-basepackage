import Vue from 'vue'
import Vuex, { Store }  from 'vuex'
Vue.use(Vuex)

import { IUserinfo } from './modules/userinfo'

export interface IRootState {
  userinfo: IUserinfo
}

// Declare empty store first, dynamically register all modules later.
const store: Store<IRootState> = new Vuex.Store<IRootState>({})
export default store
