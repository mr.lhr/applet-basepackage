import { storeKey } from '@/env'

export interface storeTemp {
  key: string
  value?: string | object
}

class Store {
  public prefix: string

  constructor() {
    this.prefix = storeKey
  }
	
  async set({ key, value = "" }: storeTemp) {
    uni.setStorageSync(`${this.prefix}${key}`, typeof value == "object" ? JSON.stringify(value) : value)
    return true
  }

  get({ key }: storeTemp) {
    if (!key) { throw new Error('没有找到key。') }
    if (typeof key === 'object') { throw new Error('key不能是一个对象。') }
    let value = uni.getStorageSync(`${this.prefix}${key}`)
    if (value && value !== null) { value = JSON.parse(value) }
    return value
  }

  remove({ key }: storeTemp) {
    uni.removeStorageSync(`${this.prefix}${key}`)
  }

}

const store = new Store()
export default store
