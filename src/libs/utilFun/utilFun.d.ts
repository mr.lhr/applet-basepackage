/**
 * 通用方法
 * create: lihuarong<517849815@qq.com>
 * time: 2020-07-16 16:26:49
 */
declare module _default {
  interface enumfuns {
    /**
     * 性别转换
     */
    _gender(data: string | number): string,
    /**
     * 奖品卷状态
     */
    _prizeCouponstatus(data: string): string,
    /**
     * 奖品类型
     */
    _type(data: string): string,
    /**
     * 金额转化 .00
     */
    _changeMoney(data: number): string,
    /**
     * 日期字符串转化时间戳
     */
    _changeDateTime(data: string): number,
    /**
     * 性别转number
     */
    _genderToNumber(data: string): number,
    /**
     * 酒量转换
     */
    _drinkChange(data: number): string,
    /**
     * 图片转换
     */
    _changeImage(image: string): string,
    /**
     * 日期转化 - 转成点
     */
    _dateStrLineToPoint(str: string, isTime: boolean): string
  }
}
