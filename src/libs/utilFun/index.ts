/**
 * 通用函数
 * create: lihuarong<517849815@qq.com>
 * time: 2020-07-16 16:26:49
 */
import Vue from 'vue'

class Enumfuns {
  private gender = {
    0: '未知',
    未知: 0,
    1: '男',
    男: 1,
    2: '女',
    女: 2
  } as any

  /**
   * 性别转化
   */
  _gender(data: string | number) {
    return this.gender[data] || '未知'
  }

  /**
   * 性别转化为数字
   */
  _genderToNumber(data: string | number) {
    return this.gender[data] || '男'
  }

  private prizeCouponstatus = {
    'un-cash': '未兑换',
    'await-send': '待发货',
    done: '已完成',
    expired: '已过期'
  } as any

  /**
   * 奖品
   */
  _prizeCouponstatus(data: string) {
    return this.prizeCouponstatus[data] || '未知'
  }

  private type = {
    material: '实物',
    virtual: '虚拟'
  } as any

  /**
   * 商品类型
   */
  _type(data: string) {
    return this.type[data] || '未知'
  }

  /**
   * 金额转化
   */
  _changeMoney(data: number | string) {
    if (!data) return 0
    return (parseInt(`${data}`) / 100).toFixed(2)
  }

  /**
    * 时间戳
    */
  _changeDateTime(data: string): number {
    if (!data) return -1
    const newDataStr = data.replace(/\.|\u002d/g, '/')
    const date: any = new Date(newDataStr)
    const timestamp = Date.parse(date)
    return timestamp
  }

  /**
    * 数组加小数点
    */
  numberCPoint(data: number) {
    const isPoint = `${data}`.indexOf('.')
    if (isPoint === -1) return `${data}.0`
    return `${data}`.substr(0, isPoint + 2)
  }

  /**
    * 酒量换算
    */
  _drinkChange(data: number) {
    if (!data) return '0两'
    if (data < 500) return `${this.numberCPoint(data / 50)}两`
    return `${this.numberCPoint(data / 500)}斤`
  }

  /**
    * 富文本图片自适应
    */
  _richchange(img: string) {
    return img.replace(/<img/gi, '<img style="width:100%;height:auto"')
  }

  /**
    * 图片,分割
    */
  _changeImage(image: string) {
    if (!image) return ''
    return image.split(',')[0]
  }

  /**
  * 日期转化
  * @param str
  * @param isTime
  */
  _dateStrLineToPoint(str: string, isTime: boolean) {
    if (isTime === undefined) isTime = true
    if (!str || typeof str !== 'string') return ''
    const now = new Date()
    const time = this._changeDateTime(str)
    if (time === -1) return ''
    const diff = Date.parse(now as any) - time
    str = str.replace(/-/g, '.')
    if (!isTime) str = str.split(' ')[0]
    if (diff < 3600000) str = `${parseInt(`${diff / 60000}`)}分钟前`
    if (diff >= 3600000 && diff < 24 * 3600000) str = `${parseInt(`${diff / 3600000}`)}小时前`
    if (diff >= 24 * 3600000 && diff < 48 * 3600000) str = `昨天${new Date(time).getHours()}:${new Date(time).getMinutes()}`
    return str
  }
}

// 将通用转化挂载到vue的原型上
const value = new Enumfuns()
Object.defineProperty(Vue.prototype, '$ENUM', { value })

export default Enumfuns
