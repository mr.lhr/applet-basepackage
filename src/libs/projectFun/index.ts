/**
 * 通用函数
 * create: lihuarong<517849815@qq.com>
 * time: 2020-07-16 16:26:49
 */
import Vue from 'vue'

class PEnumfuns {
  
}

// 将通用转化挂载到vue的原型上
const value = new PEnumfuns()
Object.defineProperty(Vue.prototype, '$PENUM', { value })

export default PEnumfuns
