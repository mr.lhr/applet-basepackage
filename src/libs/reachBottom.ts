/**
 * 通用下拉数据处理
 * by: lihuarong<517849815@qq.com>
 * time: 2020年7月9日 09点46分
 */

 export interface objMode {
  postData: any,
  list: any,
  total: number
 }

 class handles {
  private obj = {
    postData: { currentPage: 0 },
    list: [],
    total: 0
  }

  constructor(obj: objMode) {
    if(obj.total === undefined) obj.total = 0
    this.obj = obj
  }

  /**
   * 处理请求对象
   */
  changePage(idAdd?: boolean) {
    idAdd ? this.obj.postData.currentPage += 1 : this.obj.postData.currentPage -= 1
  }

  /**
   * 查询处理
   */
  listHandle(res: any, isConcat ?: boolean) {
    const { rows, data, total, totalCount } = res.data
    const totalEnd = total || totalCount
    if (this.obj.list.length == totalEnd && isConcat) return this.changePage()
    this.obj.total = total
    const dataEnd = data || rows
    this.obj.list = isConcat ? [...this.obj.list, ...dataEnd] : dataEnd
  }

  /**
   * 查询
   * @param { getList：查询函数, isRef：是否重置, isConcat: 是否追加  } 
   */
  async getList({ getList, isRef = false, isConcat = false }: { getList: any, isRef: boolean, isConcat: boolean }) {
    if (!getList) return false
    if (isRef) this.obj.postData.currentPage = 0
    this.changePage(true)
    const res: any = await getList()
    if(!res || (res.code != '0000' && res.code !== 200)) {
      this.changePage()
      return false
    }
    this.listHandle(res, isConcat)
    return true
  }

 }


 export const reachBottom = handles
