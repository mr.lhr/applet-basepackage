import { uploadVal } from '../env'
const COS = require("./cos-wx-sdk-v5.js")

let cos = new COS({
  SecretId: "",
  SecretKey: ""
})

export const upload = function (file: any, key?: string): Promise<string> {
  return new Promise((resolve, reject) => {
    const date = new Date()
    const math = parseInt(`${Math.random() * 100000}`)
    const name = `${date.getTime()}${math}`
    cos.postObject(
      {
        Bucket: uploadVal.Bucket /* 必须 */,
        Region: "ap-guangzhou" /* 存储桶所在地域，必须字段 */,
        Key: `/head/${name}` /* 必须 */,
        FilePath: file, // 上传文件对象
      }, (err:any, data:any) => {
        if (err) {
          //图片上传shibai
          console.info(err)
          return reject('')
        }
        resolve(data.headers.Location)
      }
    )
  })
}