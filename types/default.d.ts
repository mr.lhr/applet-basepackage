/**
 * 通用
 * create: lihuarong
 * time: 2020年4月28日 11点10分
 */
declare namespace _default {
  /**
   * 通用返回值
   */
  interface post {
    // 请求系统编码
    reqCode?: string,

    // 请求时间
    reqTime?: string,
  }

  /**
   * 通用返回值
   */
  interface res {
    // 请求返回码
    code?: string,

    // 处理信息
    msg?: string,

    // 时间戳
    timespan?: number
  }

  /**
   * 分页通用
   */
  interface listPagePost extends post {
    // 页码
    currentPage: number,

    // 页面大小
    showCount: number
  }

  interface apifuns {}

  interface routerList {}

  interface enumfuns {}
}