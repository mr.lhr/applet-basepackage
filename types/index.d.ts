import Vue from 'vue'
declare module "*.vue" {
  import Vue from 'vue'
  export default Vue
}

///<reference path="./default.d.ts" />
///<reference path="../src/components/index.d.ts" />
///<reference path="../src/api/model/index.d.ts"/>
///<reference path="../src/libs/utilFun/utilFun.d.ts"/>
///<reference path="../src/libs/utilFun/project.d.ts"/>

declare module 'vue/types/vue' {
  interface Vue {
    $API: _default.apifuns;
    $ENUM: _default.enumfuns;
    $PENUM: _default.penumfuns;
  }
}
